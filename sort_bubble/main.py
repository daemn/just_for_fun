# encoding: UTF-8
import random
from copy import copy

__doc__ = """Bubble sorting algorithm."""


def bubble(sequence: list[int]) -> list[int]:
    # Алгоритм состоит из повторяющихся проходов по сортируемому массиву. За каждый проход элементы последовательно
    # сравниваются попарно и, если порядок в паре неверный, выполняется перестановка элементов. Проходы по массиву
    # повторяются N-1 раз или до тех пор, пока на очередном проходе не окажется, что обмены больше не нужны, что
    # означает — массив отсортирован. При каждом проходе алгоритма по внутреннему циклу очередной наибольший элемент
    # массива ставится на своё место в конце массива рядом с предыдущим «наибольшим элементом», а наименьший элемент
    # перемещается на одну позицию к началу массива.

    water_line: int = len(sequence) - 1
    is_sorted: bool = False
    while not is_sorted:
        is_sorted = True
        for index in range(water_line):
            if sequence[index] > sequence[index + 1]:
                sequence[index], sequence[index + 1] = sequence[index + 1], sequence[index]
                is_sorted = False
        water_line -= 1
    return sequence


if __name__ == '__main__':
    random_sequence: list[int] = [random.randint(10, 99) for _ in range(100)]
    valid_result: list[int] = sorted(random_sequence)
    assert bubble(copy(random_sequence)) == valid_result
