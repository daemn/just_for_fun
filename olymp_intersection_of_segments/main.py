#!/usr/bin/env python3
# encoding: UTF-8

# There are two line segments. Check whether they are crossing or not.

from dataclasses import dataclass


@dataclass
class Segment:
    x1: int
    y1: int
    x2: int
    y2: int


def are_crossing(seg_a: Segment, seg_b: Segment) -> bool:
    #   System of equations:               Let's:               So:
    # | xc = ax1 + ak * ( ax2 - ax1 )    | axd = ax2 - ax1    | xc = ax1 + ak * axd
    # | yc = ay1 + ak * ( ay2 - ay1 )    | ayd = ay2 - ay1  =>| yc = ay1 + ak * ayd
    # | xc = bx1 + bk * ( bx2 - bx1 )    | bxd = bx2 - bx1    | xc = bx1 + bk * bxd
    # | yc = by1 + bk * ( by2 - by1 )    | byd = by2 - by1    | yc = by1 + bk * byd
    #
    # A formula for ak:
    # ax1 + ak * axd = bx1 + bk * bxd
    # ak = ( bk * bxd + bx1 - ax1 ) / axd
    #
    # A formula for bk:
    # by1 + bk * byd = ay1 + ak * ayd
    # bk = ( ay1 + ak * ayd - by1 ) / byd
    #
    # Replace ak with the formula:
    # bk = ( ay1 + ( ( bk * bxd + bx1 - ax1 ) / axd ) * ayd - by1 ) / byd
    # bk = ( ( bk * bxd * ayd + ayd * ( bx1 - ax1 ) ) / axd + ay1 - by1 ) / byd
    # bk = ( bk * bxd * ayd / axd + ayd * ( bx1 - ax1 ) / axd + ay1 - by1 ) / byd
    # bk = bk * bxd * ayd / axd * byd + ayd * ( bx1 - ax1 ) / axd * byd + ( ay1 - by1 ) / byd
    # bk - bk * bxd * ayd / axd * byd = ayd * ( bx1 - ax1 ) / axd * byd + ( ay1 - by1 ) / byd
    # bk * axd * byd - bk * bxd * ayd = ayd * ( bx1 - ax1 ) * axd * byd / axd * byd + ( ay1 - by1 ) * axd * byd / byd
    # bk * axd * byd - bk * bxd * ayd = ayd * ( bx1 - ax1 ) + ( ay1 - by1 ) * axd
    # bk * ( axd * byd - bxd * ayd ) = ayd * ( bx1 - ax1 ) + ( ay1 - by1 ) * axd
    # bk = ( ayd * ( bx1 - ax1 ) + ( ay1 - by1 ) * axd ) / ( axd * byd - bxd * ayd )
    #
    # Return original variables:
    # bk = ( ( ay2 - ay1 ) * ( bx1 - ax1 ) + ( ay1 - by1 ) * ( ax2 - ax1 ) )
    #   / ( ( ax2 - ax1 ) * ( by2 - by1 ) - ( bx2 - bx1 ) * ( ay2 - ay1 ) )
    try:
        bk: float = (((seg_a.y2 - seg_a.y1) * (seg_b.x1 - seg_a.x1) + (seg_a.y1 - seg_b.y1) * (seg_a.x2 - seg_a.x1))
                     / ((seg_a.x2 - seg_a.x1) * (seg_b.y2 - seg_b.y1) - (seg_b.x2 - seg_b.x1) * (seg_a.y2 - seg_a.y1)))
    except ZeroDivisionError:
        bk: float = 0.0
    try:
        ak: float = (bk * (seg_b.x2 - seg_b.x1) + seg_b.x1 - seg_a.x1) / (seg_a.x2 - seg_a.x1)
    except ZeroDivisionError:
        ak: float = 0.0
    return (0 <= bk <= 1) and (0 <= ak <= 1)


if __name__ == '__main__':
    #    0123456789...
    #
    # 0  c4,0*-
    # 1        --
    # 2          --
    # 3  a4,3*     --
    # 4       |      -*13,4
    # 5        |  ---*12,5
    # 6       --+-           cross_point ~ 7.0,5.9
    # 7   *---   |
    # 8  b1,7     |
    # 9            *10,9

    a = Segment(x1=4, y1=3, x2=10, y2=9)
    b = Segment(x1=1, y1=7, x2=12, y2=5)
    c = Segment(x1=4, y1=0, x2=13, y2=4)

    assert are_crossing(a, b) is True
    assert are_crossing(c, a) is False
    assert are_crossing(c, b) is False
