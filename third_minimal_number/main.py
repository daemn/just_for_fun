# encoding: UTF-8
from copy import copy

# There is a sequence of random numbers. Find the third minimal number.


def simple_search(sequence: list[int]) -> int:
    """Simplest method."""
    min_set: list[int] = [sequence[0]]
    for value in sequence[1:]:
        if value not in min_set and (value < min_set[-1] or len(min_set) < 3):
            min_set.append(value)
            min_set.sort()
            min_set = min_set[:3]

    return min_set[-1]


def weird_search(sequence: list[int]) -> int:
    """Weird method."""
    min_set: list[int] = [sequence[0]]
    for value in sequence[1:]:
        if value not in min_set and (value < min_set[-1] or len(min_set) < 3):
            insert_index: int = 0
            while insert_index < len(min_set):
                if value < min_set[insert_index]:
                    break
                insert_index += 1
            min_set.insert(insert_index, value)
            min_set = min_set[:3]

    return min_set[-1]


if __name__ == '__main__':
    source_data: list = [3, 3, 8, 5, 7, 2, 9, 4, 6]
    assert simple_search(copy(source_data)) == 4
    assert weird_search(copy(source_data)) == 4
