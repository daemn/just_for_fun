import dataclasses
import pytest
import typing as t
import enum

__doc__ = """There is a city map with vertical and horizontal streets. Each crossroad has a unique number. Each street
begins at a crossroad and ends at the next crossroad, so each street is defined by a crossroad pair. Streets may have
road signs at the beginning such as "no passage", "weight limit 5 tons" and at the end "right turn prohibited", "right
turn required". If there is no sign on the street, it may not be in the input data.

Find any valid way from crossroad A to crossroad B for a car with a given weight.
"""

# Example:
#
# Signs:
# s1 - "no passage"
# s2 - "weight limit 5 tons"
# s3 - "right turn prohibited"
# s4 - "right turn required"
#
# City map:
#     0      1      2      3      4
#    ────────v──────────────────────
#  0│ 1      2      3      4 s2   5 │
#   │   ┌──┐ · ┌──┐   ┌──┐   ┌──┐   │
#   │   │  │ · │  │   │  │   │  │   │
#   │   └──┘s4 └──┘   └──┘s1 └──┘   │
#  1│ 6      7···s3·8      9     10 │
#   │   ┌──┐   ┌──┐ · ┌──┐   ┌──┐   │
#   │   │  │   │  │ · │  │   │  │   │
#   │   └──┘   └──┘ · └──┘   └──┘   │
#  2│11     12     13 s1  14·····15 >
#   │   ┌──┐   ┌──┐ · ┌──┐ · ┌──┐   │
#   │   │  │   │  │ · │  │ · │  │   │
#   │   └──┘   └──┘ · └──┘ · └──┘   │
#  3│16     17     18·····19 s2  20 │
#    ───────────────────────────────
#
# Input:
# {
#     "car_weight": 3,
#     "start": 2,
#     "end": 15,
#     "map_width": 5,
#     "map_height": 4,
#     "streets": [
#         {"from":  7, "to":  8, "signs": [3]},
#         {"from":  4, "to":  5, "signs": [2]},
#         {"from": 13, "to": 14, "signs": [1]},
#         {"from": 19, "to": 20, "signs": [2]},
#         {"from":  2, "to":  7, "signs": [4]},
#         {"from":  9, "to":  4, "signs": [1]},
#     ]
# }
#
# Output:
# [2, 7, 8, 13, 18, 19, 14, 15]


class Sign(enum.Enum):
    no_sign = 'no sign'
    no_pass = 'no passage'
    limit = 'weight limit 5 tons'
    no_right = 'right turn prohibited'
    right = 'right turn required'


@dataclasses.dataclass(frozen=True)
class Crossroad:
    x: int
    y: int


@dataclasses.dataclass(frozen=True)
class Street:
    from_cr: Crossroad
    to_cr: Crossroad


class CityMap:
    all_signs: dict[Street, list[Sign]]
    width: int
    height: int
    _total_crossroads: int

    _dual_direction_signs: tuple = (Sign.no_pass, Sign.limit)

    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height
        self.all_signs = {}
        self._total_crossroads = width * height

    def crossroad_to_number(self, cr: Crossroad) -> int:
        return cr.y * self.width + cr.x + 1

    def number_to_crossroad(self, cr_num: int) -> Crossroad:
        y, x = divmod(cr_num - 1, self.width)
        return Crossroad(x, y)

    @staticmethod
    def _reverse_street(street: Street) -> Street:
        return Street(street.to_cr, street.from_cr)

    def add_sign(self, street: Street, sign: Sign):
        self.all_signs.setdefault(street, []).append(sign)
        if sign in self._dual_direction_signs:
            self.all_signs.setdefault(self._reverse_street(street), []).append(sign)

    def get_signs_on_the_street(self, street: Street) -> list[Sign]:
        return self.all_signs.get(street, [])

    @staticmethod
    def is_turn_right(came_from: Crossroad, current: Crossroad, head_to: Crossroad) -> bool:
        if (
                came_from.y == current.y
                and current.x == head_to.x
                and head_to.y - current.y == current.x - came_from.x
        ):
            return True
        if (
                came_from.x == current.x
                and current.y == head_to.y
                and current.x - head_to.x == current.y - came_from.y
        ):
            return True
        return False

    def get_right_turn(self, came_from: Crossroad, current: Crossroad) -> t.Optional[Street]:
        if came_from.y == current.y:
            next_cr = Crossroad(current.x, current.y + current.x - came_from.x)
        else:
            next_cr = Crossroad(current.x + came_from.y - current.y, current.y)

        if (0 <= next_cr.x < self.width) and (0 <= next_cr.y < self.height):
            return Street(from_cr=current, to_cr=next_cr)
        return None

    def is_passage_allowed(self, car_weight: int, street: Street) -> bool:
        return (
                Sign.no_pass not in self.get_signs_on_the_street(street)
                and not (Sign.limit in self.get_signs_on_the_street(street) and car_weight >= 5)
        )

    def all_ways_from_here(self, car_weight: int, current: Crossroad, came_from: Crossroad = None) -> list[Street]:
        if came_from is None:
            right_turn_required = False
            right_turn_prohibited = False
        else:
            right_turn_required = Sign.right in self.get_signs_on_the_street(Street(came_from, current))
            right_turn_prohibited = Sign.no_right in self.get_signs_on_the_street(Street(came_from, current))

        proposed_streets: list[Street]
        if right_turn_required:
            proposed_streets = [self.get_right_turn(came_from, current)]
        else:
            proposed_streets = []
            for dx, dy in (0, -1), (0, 1), (-1, 0), (1, 0):
                next_x: int = current.x + dx
                next_y: int = current.y + dy
                if (0 <= next_x < self.width) and (0 <= next_y < self.height):
                    proposed_streets.append(Street(current, Crossroad(next_x, next_y)))

        streets: list[Street] = []
        for street in proposed_streets:
            if right_turn_prohibited and self.is_turn_right(came_from, current, street.to_cr):
                continue
            if not self.is_passage_allowed(car_weight=car_weight, street=street):
                continue
            streets.append(street)

        return streets


def sort_with_priority(current: Crossroad, target: Crossroad, streets: list[Street]) -> list[Street]:
    dx: int = target.x - current.x
    dy: int = target.y - current.y
    return sorted(
        streets,
        key=lambda a: (dx * (a.to_cr.x - a.from_cr.x), dy * (a.to_cr.y - a.from_cr.y)),
        reverse=True
    )


def find_the_way(
        city_map: CityMap,
        current: Crossroad,
        target: Crossroad,
        car_weight: int,
        came_from: Crossroad = None,
        locked_streets: list[Street] = None
) -> list[int]:
    if locked_streets is None:
        locked_streets = []

    if current == target:
        return [city_map.crossroad_to_number(current)]

    streets: list[Street] = city_map.all_ways_from_here(car_weight=car_weight, current=current, came_from=came_from)
    streets = sort_with_priority(current=current, target=target, streets=streets)
    for street in streets:
        if street in locked_streets:
            continue
        way: list[int] = find_the_way(
            city_map=city_map,
            current=street.to_cr,
            target=target,
            car_weight=car_weight,
            came_from=current,
            locked_streets=locked_streets + [street]
        )
        if way is not None:
            return [city_map.crossroad_to_number(current)] + way


def main():
    """The demo entry point."""
    city_map = CityMap(width=6, height=9)
    city_map.add_sign(Street(Crossroad(2, 3), Crossroad(3, 3)), Sign.no_pass)
    way: list[int] = find_the_way(
        city_map=city_map,
        current=Crossroad(3, 8),
        target=Crossroad(1, 0),
        car_weight=7
    )
    print(way)


if __name__ == '__main__':
    main()


@pytest.fixture
def raw_input_data() -> dict:
    return {
        'car_weight': 3,
        'start': 2,
        'end': 15,
        'map_width': 5,
        'map_height': 4,
        'streets': [
            {'from': 7, 'to': 8, 'signs': [3, 2]},
            {'from': 13, 'to': 14, 'signs': [1]},
            {'from': 19, 'to': 20, 'signs': [2]},
            {'from': 4, 'to': 5, 'signs': [2]},
            {'from': 2, 'to': 7, 'signs': [4]},
            {'from': 7, 'to': 2, 'signs': [2]},
            {'from': 9, 'to': 4, 'signs': [1]},
        ]
    }


@pytest.fixture
def city_map_for_test(raw_input_data: dict) -> CityMap:
    city_map = CityMap(width=raw_input_data.get('map_width'), height=raw_input_data.get('map_height'))
    all_signs = {0: Sign.no_sign, 1: Sign.no_pass, 2: Sign.limit, 3: Sign.right, 4: Sign.no_right}
    for raw_street in raw_input_data.get('streets', []):
        for sign in raw_street['signs']:
            city_map.add_sign(
                street=Street(
                    city_map.number_to_crossroad(raw_street['from']), city_map.number_to_crossroad(raw_street['to'])
                ),
                sign=all_signs[sign]
            )
    return city_map


def test__crossroad_to_number():
    city_map = CityMap(5, 4)
    assert city_map.crossroad_to_number(Crossroad(0, 0)) == 1
    assert city_map.crossroad_to_number(Crossroad(4, 1)) == 10
    assert city_map.crossroad_to_number(Crossroad(0, 3)) == 16


def test__number_to_crossroad():
    city_map = CityMap(5, 4)
    assert city_map.number_to_crossroad(1) == Crossroad(0, 0)
    assert city_map.number_to_crossroad(15) == Crossroad(4, 2)
    assert city_map.number_to_crossroad(16) == Crossroad(0, 3)


def test__get_signs_on_the_street(city_map_for_test: CityMap):
    f = city_map_for_test.get_signs_on_the_street
    assert f(Street(Crossroad(1, 1), Crossroad(2, 1))) == [Sign.right, Sign.limit]
    assert f(Street(Crossroad(1, 0), Crossroad(1, 1))) == [Sign.no_right, Sign.limit]
    assert f(Street(Crossroad(3, 0), Crossroad(3, 1))) == [Sign.no_pass]
    assert f(Street(Crossroad(1, 0), Crossroad(2, 0))) == []


def test__is_turn_right(city_map_for_test: CityMap):
    f = city_map_for_test.is_turn_right
    assert f(Crossroad(1, 1), Crossroad(2, 1), Crossroad(2, 2)) is True
    assert f(Crossroad(4, 2), Crossroad(4, 3), Crossroad(3, 3)) is True
    assert f(Crossroad(1, 2), Crossroad(0, 2), Crossroad(0, 1)) is True

    assert f(Crossroad(2, 1), Crossroad(3, 1), Crossroad(3, 0)) is False
    assert f(Crossroad(0, 3), Crossroad(0, 2), Crossroad(0, 1)) is False
    assert f(Crossroad(1, 2), Crossroad(2, 2), Crossroad(2, 1)) is False
    assert f(Crossroad(3, 2), Crossroad(3, 1), Crossroad(2, 1)) is False


def test__get_right_turn(city_map_for_test: CityMap):
    f = city_map_for_test.get_right_turn
    assert f(Crossroad(1, 1), Crossroad(2, 1)) == Street(Crossroad(2, 1), Crossroad(2, 2))
    assert f(Crossroad(4, 2), Crossroad(4, 3)) == Street(Crossroad(4, 3), Crossroad(3, 3))
    assert f(Crossroad(1, 2), Crossroad(0, 2)) == Street(Crossroad(0, 2), Crossroad(0, 1))

    assert f(Crossroad(3, 3), Crossroad(4, 3)) is None
    assert f(Crossroad(3, 0), Crossroad(2, 0)) is None
    assert f(Crossroad(4, 2), Crossroad(4, 1)) is None
    assert f(Crossroad(0, 0), Crossroad(0, 1)) is None


def test__is_passage_allowed(city_map_for_test: CityMap):
    f = city_map_for_test.is_passage_allowed
    assert f(car_weight=3, street=Street(Crossroad(1, 1), Crossroad(1, 0))) is True
    assert f(car_weight=3, street=Street(Crossroad(1, 1), Crossroad(2, 1))) is True
    assert f(car_weight=3, street=Street(Crossroad(1, 3), Crossroad(2, 3))) is True

    assert f(car_weight=10, street=Street(Crossroad(1, 1), Crossroad(1, 0))) is False
    assert f(car_weight=3, street=Street(Crossroad(2, 2), Crossroad(3, 2))) is False
    assert f(car_weight=3, street=Street(Crossroad(3, 2), Crossroad(2, 2))) is False


def test__all_ways_from_here(city_map_for_test: CityMap):
    f = city_map_for_test.all_ways_from_here
    assert list(f(car_weight=10, current=Crossroad(3, 3))) == [
        Street(Crossroad(3, 3), Crossroad(3, 2)),
        Street(Crossroad(3, 3), Crossroad(2, 3))
    ]
    assert list(f(car_weight=3, current=Crossroad(4, 0))) == [
        Street(Crossroad(4, 0), Crossroad(4, 1)),
        Street(Crossroad(4, 0), Crossroad(3, 0))
    ]
    assert list(f(car_weight=3, current=Crossroad(1, 2), came_from=Crossroad(0, 2))) == [
        Street(Crossroad(1, 2), Crossroad(1, 1)),
        Street(Crossroad(1, 2), Crossroad(1, 3)),
        Street(Crossroad(1, 2), Crossroad(0, 2)),
        Street(Crossroad(1, 2), Crossroad(2, 2))
    ]
    assert list(f(car_weight=3, current=Crossroad(3, 1), came_from=Crossroad(4, 1))) == [
        Street(Crossroad(3, 1), Crossroad(3, 2)),
        Street(Crossroad(3, 1), Crossroad(2, 1)),
        Street(Crossroad(3, 1), Crossroad(4, 1))
    ]
    assert list(f(car_weight=3, current=Crossroad(1, 1), came_from=Crossroad(1, 0))) == [
        Street(Crossroad(1, 1), Crossroad(1, 0)),
        Street(Crossroad(1, 1), Crossroad(1, 2)),
        Street(Crossroad(1, 1), Crossroad(2, 1))
    ]


def test__find_the_way(raw_input_data: dict, city_map_for_test: CityMap):
    way: list[int] = find_the_way(
        city_map=city_map_for_test,
        current=city_map_for_test.number_to_crossroad(raw_input_data['start']),
        target=city_map_for_test.number_to_crossroad(raw_input_data['end']),
        car_weight=raw_input_data['car_weight'],
    )
    assert way is not None
    assert way[0] == raw_input_data['start']
    assert way[-1] == raw_input_data['end']
