# encoding: UTF-8
# There are k people.
# Some of them know some of them.
# There is a person whom everyone knows, but he knows no one. He is a celebrity.
# We can ask anybody whether he knows any person or not.
# We need to find a celebrity with a minimum of requests.


def search4(matrix: dict[str, list]) -> str:

    def do_knows(one: str, other: str) -> bool:
        print(f'Do {one} knows {other}?')
        return other in matrix[one]

    suspects: list = list(matrix.keys())
    current_suspect: str = suspects.pop(0)

    while suspects:
        contact: str = suspects.pop(0)
        if do_knows(current_suspect, contact):
            current_suspect = contact

    return current_suspect


def search3(connections: list) -> int:
    """Отсеивает всех, кто знает хоть кого-то. Метод двух указателей."""
    # Taken from https://youtu.be/xGvQN_g-JCI

    def is_connected(a, b) -> bool:
        print(f'Do {a} knows {b}?')
        return connections[a - 1][b - 1] == 1

    population: int = len(connections)
    may_be: list = [a for a in range(1, population + 1)]
    lcursor = 0
    rcursor = len(may_be) - 1
    while lcursor != rcursor:
        if is_connected(may_be[lcursor], may_be[rcursor]):
            lcursor += 1
        else:
            rcursor -= 1
    return may_be[lcursor]


if __name__ == '__main__':
    assert search4({
       'Karl':   ['Moe', 'Lenny'],
       'Lenny':  ['Moe', 'Karl'],
       'Moe':    [],
       'Homer':  ['Moe', 'Karl'],
       'Barney': ['Moe', 'Lenny', 'Homer'],
    }) == 'Moe'

    assert search3([
        [0, 1, 1, 0, 0],
        [1, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [1, 0, 1, 0, 0],
        [0, 1, 1, 1, 0]
    ]) == 3
