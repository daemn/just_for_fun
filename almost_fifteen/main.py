import sys
import time

from tools import Coords, Move, Board, read_board_data_from_file, save_history_to_file, get_home_cell

__doc__ = """
"""


class NoAvailableMoves(Exception):
    """No available moves."""


def get_all_moves(cell: Coords) -> list[Move]:
    moves: list[Move] = []
    for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
        shift_x: int = cell.x + dx
        shift_y: int = cell.y + dy
        if not (0 <= shift_x <= 3) or not (0 <= shift_y <= 3):
            continue
        cell_shift = Coords(shift_x, shift_y)
        moves.append(Move(cell, cell_shift))
    return moves


def determine_moves(board: Board, cell: Coords, target_cell: Coords, frozen_cells: list[Coords]) -> list[Move]:
    assert cell != target_cell, 'WTF. It is not possible to move to the same cell.'

    # Determine best moves.
    suggest_moves: list[Move] = get_all_moves(cell)
    suggest_moves = list(filter(lambda a: a.to not in frozen_cells, suggest_moves))
    if not suggest_moves:
        raise NoAvailableMoves(f'Chip {board.get_chip(cell)} cannot move towards {target_cell}.')
    min_steps: int = min(map(lambda a: count_steps(a.to, target_cell), suggest_moves))
    suggest_moves = list(filter(lambda a: count_steps(a.to, target_cell) == min_steps, suggest_moves))
    moves_to_free_cell: list[Move] = list(filter(lambda a: board.get_chip(a.to) == 0, suggest_moves))
    if moves_to_free_cell:
        return [Move(cell, moves_to_free_cell[0].to)]
    next_cell: Coords = suggest_moves[0].to

    # If some chip blocks the way, then find the way to move this chip.
    free_cells: list[Coords] = board.get_free_cells()
    free_cells = list(filter(lambda a: a not in frozen_cells, free_cells))
    free_cells.sort(key=lambda a: count_steps(a, next_cell))
    return determine_moves(
        board=board, cell=next_cell, target_cell=free_cells[0], frozen_cells=frozen_cells + [cell]
    ) + [Move(cell, next_cell)]


def count_steps(cell1: Coords, cell2: Coords) -> int:
    return abs(cell1.x - cell2.x) + abs(cell1.y - cell2.y)


def apply_moves(board: Board, moves: list[Move]):
    for move in moves:
        # print(f'Move chip {board.get_chip(move.from_)} from {move.from_} to {move.to}')
        board.pprint(move)
        print()
        board.move_chip(move)
        time.sleep(0.1)
        board.pprint()
        print()
        time.sleep(0.3)


def main():
    board = Board(read_board_data_from_file('in.txt'))

    while not board.is_board_solved():
        frozen_cells: list[Coords] = []
        for active_chip in range(1, 15):
            home_cell: Coords = get_home_cell(active_chip)
            print(f'Move chip {active_chip} to home')
            while board.get_cell(active_chip) != home_cell:
                try:
                    # Free home cell.
                    if board.get_chip(home_cell) != 0:
                        print(f'Make the cell {home_cell} free')
                        free_cells: list[Coords] = board.get_free_cells()
                        moves: list[Move] = determine_moves(
                            board=board, cell=home_cell, target_cell=free_cells[0], frozen_cells=frozen_cells
                        )
                        apply_moves(board=board, moves=moves)

                    current_cell: Coords = board.get_cell(active_chip)
                    if count_steps(current_cell, home_cell) == 1:
                        # The active chip can reach the home cell in one step.
                        print(f'Move the chip {board.get_chip(current_cell)} to the home cell {home_cell}')
                        moves: list[Move] = determine_moves(
                            board=board, cell=current_cell, target_cell=home_cell, frozen_cells=frozen_cells
                        )
                        apply_moves(board=board, moves=moves)
                        frozen_cells.append(get_home_cell(active_chip))
                    else:
                        # The active chip need to make many steps. Make one.
                        print(f'Move the chip {board.get_chip(current_cell)} towards the home cell {home_cell}')
                        moves: list[Move] = determine_moves(
                            board=board, cell=current_cell, target_cell=home_cell,
                            frozen_cells=frozen_cells + [get_home_cell(active_chip)]
                        )
                        apply_moves(board=board, moves=moves)

                except NoAvailableMoves as err:
                    print(err)
                    if frozen_cells:
                        last_frozen_cell: Coords = frozen_cells.pop()
                        print(f'I try to remove the last frozen cell {last_frozen_cell}.')
                    else:
                        print(f'I am in panic so I try to skip this chip.')
                        break

    save_history_to_file(history=board.history, filename='out.txt')


class TestAllMain:
    """Test class for pytest"""

    def test__get_all_moves(self):
        assert get_all_moves(Coords(1, 1)) == [
            Move(Coords(1, 1), Coords(0, 1)),
            Move(Coords(1, 1), Coords(2, 1)),
            Move(Coords(1, 1), Coords(1, 0)),
            Move(Coords(1, 1), Coords(1, 2)),
        ]
        assert get_all_moves(Coords(3, 2)) == [
            Move(Coords(3, 2), Coords(2, 2)),
            Move(Coords(3, 2), Coords(3, 1)),
            Move(Coords(3, 2), Coords(3, 3)),
        ]
        assert get_all_moves(Coords(0, 0)) == [
            Move(Coords(0, 0), Coords(1, 0)),
            Move(Coords(0, 0), Coords(0, 1)),
        ]

    def test__count_steps(self):
        assert count_steps(Coords(0, 0), Coords(0, 1)) == 1
        assert count_steps(Coords(1, 2), Coords(3, 1)) == 3


if __name__ == '__main__':
    main()
    sys.exit(0)
