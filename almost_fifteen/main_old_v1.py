import dataclasses
import pytest

from tools import Coords, Move, determine_sign

__doc__ = """
"""


@dataclasses.dataclass
class RatedMove:
    move: Move
    chip: int
    rate: int = 0


class Attempts:
    _data: dict[str, set[Move]]

    def __init__(self):
        """Keeps all tried moves."""
        self._data = {}

    def add(self, stage_key: str, move: Move):
        """Adds new move in history."""
        self._data.setdefault(stage_key, set()).add(move)

    def remove(self, stage_key: str, move: Move):
        """Removes the move from history."""
        self._data[stage_key].remove(move)
        if not self._data[stage_key]:
            del self._data[stage_key]

    def is_repeat(self, stage_key: str, move: Move) -> bool:
        """Returns True if this move has already taken place at this stage."""
        return move in self._data.get(stage_key, set())


class Board:
    _data: list[list[int]]
    _history: list[Move]
    _attempts: Attempts

    def __init__(self, data: list[list[int]]):
        self._data = data
        self._history = []
        self._attempts = Attempts()

    def is_sorted(self) -> bool:
        """Returns True if game is solved."""
        return self._data == [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 0, 0]]

    def move_chip(self, from_: Coords, to: Coords):
        """Moves chip."""
        if self._data[from_.y][from_.x] == 0:
            raise RuntimeError(f'Try to move empty cell {from_}.')
        if self._data[to.y][to.x] > 0:
            raise RuntimeError(f'Try to move chip from {from_} to non free cell {to}.')
        stage_key = str(self._data)
        move = Move(from_, to)
        if self._attempts.is_repeat(stage_key, move):
            raise RuntimeError(f'Try to repeat old move.')

        self._attempts.add(stage_key=stage_key, move=move)
        self._history.append(move)
        self._data[to.y][to.x], self._data[from_.y][from_.x] = self._data[from_.y][from_.x], 0

    def back(self):
        """Undo one move."""
        move: Move = self._history.pop(-1)
        self._data[move.to.y][move.to.x], self._data[move.from_.y][move.from_.x] = 0, self._data[move.to.y][move.to.x]
        self._attempts.remove(stage_key=str(self._data), move=move)

    def export_history(self) -> tuple:
        """Returns moves history."""
        return tuple(map(dataclasses.astuple, self._history))

    def _find_zeroes(self) -> list[Coords]:
        """Returns coords of free cells."""
        result: list[Coords] = []
        for y in range(4):
            for x in range(4):
                if self._data[y][x] == 0:
                    result.append(Coords(x, y))
        return result

    @staticmethod
    def _home_cell(number: int) -> Coords:
        """Returns coords of cell where chip should be."""
        y, x = divmod(number - 1, 4)
        return Coords(x, y)

    @staticmethod
    def _cell_owner(cell: Coords) -> int:
        """Returns the chip that should be in this cell."""
        return cell.y * 4 + cell.x + 1

    def _sort_moves(self, moves: list[Move]) -> list[Move]:
        """Sorts moves by priority."""
        rated_moves: list[RatedMove] = [
            RatedMove(move=a, chip=self._data[a.from_.y][a.from_.x])
            for a in moves
        ]

        rated_moves.sort(key=lambda a: a.chip)
        for index, r_move in enumerate(rated_moves):

            from_: Coords = r_move.move.from_
            to: Coords = r_move.move.to
            # Chip stays on his home cell.
            if r_move.chip == self._cell_owner(from_):
                r_move.rate -= (30 - index)
                continue

            # Chip is going to move at his home cell.
            if r_move.chip == self._cell_owner(to):
                r_move.rate += 2

            home: Coords = self._home_cell(r_move.chip)
            # Chip is going to move in the right or wrong direction.
            if determine_sign(home.x - from_.x) == determine_sign(to.x - from_.x):
                r_move.rate += 1
            else:
                r_move.rate -= 2
            if determine_sign(home.y - from_.y) == determine_sign(to.y - from_.y):
                r_move.rate += 1
            else:
                r_move.rate -= 2

        rated_moves.sort(key=lambda a: a.rate, reverse=True)
        return [a.move for a in rated_moves]

    def get_available_moves(self) -> list[Move]:
        """Returns all available moves."""
        stage_key = str(self._data)
        moves: list[Move] = []
        zeroes: list[Coords] = self._find_zeroes()
        for zero_cell in zeroes:
            for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
                check_cell = Coords(zero_cell.x + dx, zero_cell.y + dy)
                if (
                        check_cell.x < 0
                        or check_cell.x > 3
                        or check_cell.y < 0
                        or check_cell.y > 3
                        or not self._data[check_cell.y][check_cell.x]
                ):
                    continue
                if self._history and self._history[-1] == Move(zero_cell, check_cell):
                    continue
                move = Move(check_cell, zero_cell)
                if not self._attempts.is_repeat(stage_key=stage_key, move=move):
                    moves.append(move)

        return self._sort_moves(moves)

    def print(self):
        """Print the board."""
        for line in self._data:
            print('{0:>2} {1:>2} {2:>2} {3:>2}'.format(*line))


def main():
    """App entry point."""
    board = Board([
        [ 1,  5,  2, 12],
        [11,  8,  0, 10],
        [14,  3, 13,  9],
        [ 7,  0,  6,  4],
    ])
    moves_stack: list[list[Move]] = []
    moves: list[Move]
    while not board.is_sorted():
        moves = board.get_available_moves()
        while not moves:
            board.back()
            moves = moves_stack.pop(-1)

        move = moves.pop(0)
        moves_stack.append(moves)
        board.move_chip(move.from_, move.to)

    print(board.export_history()[:100])


if __name__ == '__main__':
    main()


class TestAttempts:

    def test__all(self):
        att = Attempts()
        key_a, move_a = 'a', Move(Coords(1, 1), Coords(2, 1))
        key_b, move_b = 'b', Move(Coords(1, 2), Coords(2, 2))

        att.add(stage_key=key_a, move=move_a)
        assert att.is_repeat(stage_key=key_b, move=move_b) is False
        att.add(stage_key=key_b, move=move_b)
        assert att.is_repeat(stage_key=key_b, move=move_b) is True
        att.remove(stage_key=key_a, move=move_a)
        assert att.is_repeat(stage_key=key_a, move=move_a) is False


class TestBoard:

    @pytest.fixture
    def board(self) -> Board:
        return Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3, 13,  9],
            [ 7,  0,  6,  4],
        ])

    def test__is_sorted(self):
        board = Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3, 13,  9],
            [ 7,  0,  6,  4],
        ])
        assert board.is_sorted() is False

        board = Board([
            [ 1,  2,  3,  4],
            [ 5,  6,  7,  8],
            [ 9, 10, 11, 12],
            [13,  0, 14,  0],
        ])
        assert board.is_sorted() is False

        board = Board([
            [ 1,  2,  3,  4],
            [ 5,  6,  7,  8],
            [ 9, 10, 11, 12],
            [13, 14,  0,  0],
        ])
        assert board.is_sorted() is True

    def test__move_chip(self, board: Board):
        assert board._data[3][0] == 7
        assert board._data[3][1] == 0
        board.move_chip(Coords(0, 3), Coords(1, 3))
        assert board._data[3][0] == 0
        assert board._data[3][1] == 7

    def test__history(self, board: Board):
        assert board.export_history() == tuple()

        board.move_chip(Coords(1, 2), Coords(1, 3))
        board.move_chip(Coords(1, 1), Coords(1, 2))
        assert board.export_history() == (((1, 2), (1, 3)), ((1, 1), (1, 2)))

        board.back()
        assert board.export_history() == (((1, 2), (1, 3)), )

    def test__get_available_moves(self):
        board = Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3, 13,  9],
            [ 7,  0,  6,  4],
        ])
        assert sorted(board.get_available_moves(), key=lambda a: (a.from_.x, a.from_.y, a.to.x, a.to.y)) \
            == [
                   Move(Coords(x=0, y=3), Coords(x=1, y=3)),
                   Move(Coords(x=1, y=1), Coords(x=2, y=1)),
                   Move(Coords(x=1, y=2), Coords(x=1, y=3)),
                   Move(Coords(x=2, y=0), Coords(x=2, y=1)),
                   Move(Coords(x=2, y=2), Coords(x=2, y=1)),
                   Move(Coords(x=2, y=3), Coords(x=1, y=3)),
                   Move(Coords(x=3, y=1), Coords(x=2, y=1))
               ]

        board = Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3,  0,  9],
            [ 7, 13,  6,  4],
        ])
        assert sorted(board.get_available_moves(), key=lambda a: (a.from_.x, a.from_.y, a.to.x, a.to.y)) \
            == [
                   Move(Coords(x=1, y=1), Coords(x=2, y=1)),
                   Move(Coords(x=1, y=2), Coords(x=2, y=2)),
                   Move(Coords(x=2, y=0), Coords(x=2, y=1)),
                   Move(Coords(x=2, y=3), Coords(x=2, y=2)),
                   Move(Coords(x=3, y=1), Coords(x=2, y=1)),
                   Move(Coords(x=3, y=2), Coords(x=2, y=2)),
               ]
