There is a 4x4 square board with cells. 14 cells contain chips with uniq numbers (1...14) and 2 cells are free. Any chip
can be moved to a free adjacent cell. Find a way to place all chips in sorted order.

Read input data from file "in.txt". Input data saved as rows of comma separated numbers. Free cells are equal to 0.

Save solution to a file "out.txt" as comma separated numbers in format: <from_x>, <from_y>, <to_x>, <to_y>.
