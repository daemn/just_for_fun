import dataclasses
import pytest

from tools import Coords, Move, determine_sign

__doc__ = """
"""


@dataclasses.dataclass
class RatedMove:
    move: Move
    chip: int
    rate: int = 0


@dataclasses.dataclass
class Stage:
    move: Move
    avail_moves: list[Move]
    prev_key: str


class StageHistory:
    stages: dict[str, Stage]
    _tail: str = None

    def __init__(self):
        """Keeps stage information as a LIFO stack."""
        self.stages = {}

    def add(self, stage_key: str, move: Move, avail_moves: list[Move]):
        """Adds new stage data to the end of stack."""
        if stage_key in self.stages:
            raise RuntimeError(f'Try to add exists key "{stage_key}".')

        self.stages[stage_key] = Stage(move=move, avail_moves=avail_moves, prev_key=self._tail)
        self._tail = stage_key

    def is_repeat(self, stage_key: str) -> bool:
        """Returns True if the stage_key is present on the stack."""
        return stage_key in self.stages

    def back(self) -> Stage:
        """Cuts the last stage data."""
        stage: Stage = self.stages[self._tail]
        del self.stages[self._tail]
        self._tail = stage.prev_key
        return stage

    def back_to_stage(self, stage_key: str) -> list[Move]:
        """Cuts all stages from the end of the stack to stage_key."""
        cursor: str = ''
        while cursor != stage_key:
            cursor = self._tail
            stage = self.stages[self._tail]
            del self.stages[self._tail]
            self._tail = stage.prev_key

        return stage.avail_moves

    def get_moves_history(self) -> tuple:
        """Returns move history."""
        cursor: str = self._tail
        history: list = []
        while cursor is not None:
            history.append(dataclasses.astuple(self.stages[cursor].move))
            cursor = self.stages[cursor].prev_key
        history.reverse()
        return tuple(history)


class Board:
    _data: list[list[int]]

    def __init__(self, data: list[list[int]]):
        """Game "Fifteen"."""
        self._data = data

    def stage_key(self) -> str:
        """Returns uniq key of the board stage."""
        return str(self._data)

    def is_sorted(self) -> bool:
        """Returns True if game is solved."""
        return self._data == [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 0, 0]]

    def move_chip(self, from_: Coords, to: Coords):
        """Moves chip."""
        if self._data[from_.y][from_.x] == 0:
            raise RuntimeError(f'Try to move empty cell {from_}.')
        if self._data[to.y][to.x] > 0:
            raise RuntimeError(f'Try to move chip from {from_} to non free cell {to}.')

        self._data[to.y][to.x], self._data[from_.y][from_.x] = self._data[from_.y][from_.x], 0

    def _find_zeroes(self) -> list[Coords]:
        """Returns coords of free cells."""
        result: list[Coords] = []
        for y in range(4):
            for x in range(4):
                if self._data[y][x] == 0:
                    result.append(Coords(x, y))
        return result

    @staticmethod
    def _home_cell(number: int) -> Coords:
        """Returns coords of cell where chip should be."""
        y, x = divmod(number - 1, 4)
        return Coords(x, y)

    @staticmethod
    def _cell_owner(cell: Coords) -> int:
        """Returns the chip that should be in this cell."""
        return cell.y * 4 + cell.x + 1

    def _sort_moves(self, moves: list[Move]) -> list[Move]:
        """Sorts moves by priority."""
        rated_moves: list[RatedMove] = [
            RatedMove(move=a, chip=self._data[a.from_.y][a.from_.x])
            for a in moves
        ]

        rated_moves.sort(key=lambda a: a.chip)
        for index, r_move in enumerate(rated_moves):

            from_: Coords = r_move.move.from_
            to: Coords = r_move.move.to
            # Chip stays on his home cell.
            if r_move.chip == self._cell_owner(from_):
                r_move.rate -= (30 - index)
                continue

            # Chip is going to move at his home cell.
            if r_move.chip == self._cell_owner(to):
                r_move.rate += 4

            home: Coords = self._home_cell(r_move.chip)
            # Chip is going to move in the right or wrong direction.
            if determine_sign(home.x - from_.x) == determine_sign(to.x - from_.x):
                r_move.rate += 1
            else:
                r_move.rate -= 3
            if determine_sign(home.y - from_.y) == determine_sign(to.y - from_.y):
                r_move.rate += 1
            else:
                r_move.rate -= 3

        rated_moves.sort(key=lambda a: a.rate, reverse=True)
        return [a.move for a in rated_moves]

    def get_available_moves(self) -> list[Move]:
        """Returns all available moves."""
        moves: list[Move] = []
        for zero_cell in self._find_zeroes():
            for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
                check_cell = Coords(zero_cell.x + dx, zero_cell.y + dy)
                if 0 <= check_cell.x <= 3 and 0 <= check_cell.y <= 3 and self._data[check_cell.y][check_cell.x]:
                    moves.append(Move(check_cell, zero_cell))

        return self._sort_moves(moves)

    def print(self):
        """Print the board."""
        for line in self._data:
            print('{0:>2} {1:>2} {2:>2} {3:>2}'.format(*line))


def main():
    """App entry point."""
    board = Board([
        [ 1,  5,  2, 10],
        [11,  7,  0,  4],
        [14,  3, 13,  9],
        [ 8,  0,  6, 12],
    ])
    stack = StageHistory()

    print('\033[2J\033[0;0H\033[s')
    repeat_count: int = 0
    move_count: int = 0
    while not board.is_sorted():
        stage_key: str = board.stage_key()
        if stack.is_repeat(stage_key):
            repeat_count += 1
            moves = stack.back_to_stage(stage_key)
        else:
            moves = board.get_available_moves()
        while not moves:
            stage = stack.back()
            board.move_chip(stage.move.to, stage.move.from_)
            moves = stage.avail_moves

        move: Move = moves.pop(0)
        stack.add(stage_key=board.stage_key(), move=move, avail_moves=moves)
        board.move_chip(move.from_, move.to)
        move_count += 1
        print('\033[u')
        print(f'Moves checked: {move_count}; Stack length: {len(stack.stages)}; Repeats: {repeat_count}')

    print(stack.get_moves_history())


if __name__ == '__main__':
    main()


class TestStageHistory:

    def test__all(self):
        sh = StageHistory()
        key_a, move_ab, avail_moves_a = 'A', Move(Coords(1, 1), Coords(2, 1)), [Move(Coords(2, 1), Coords(3, 1))]
        key_b, move_bc, avail_moves_b = 'B', Move(Coords(1, 2), Coords(2, 2)), [Move(Coords(2, 2), Coords(3, 2))]
        key_c, move_cd, avail_moves_c = 'C', Move(Coords(1, 3), Coords(2, 3)), [Move(Coords(2, 3), Coords(3, 3))]

        assert sh.is_repeat(key_a) is False
        sh.add(key_a, move_ab, avail_moves_a)
        sh.add(key_b, move_bc, avail_moves_b)
        sh.add(key_c, move_cd, avail_moves_c)
        assert sh.is_repeat(key_a) is True

        stage: Stage = sh.back()
        assert sh.is_repeat(key_c) is False
        assert stage.move == move_cd
        assert stage.avail_moves == avail_moves_c

        sh.add(key_c, move_cd, avail_moves_c)
        avail_moves: list[Move] = sh.back_to_stage(key_b)
        assert avail_moves == avail_moves_b


class TestBoard:

    @pytest.fixture
    def board(self) -> Board:
        return Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3, 13,  9],
            [ 7,  0,  6,  4],
        ])

    def test__is_sorted(self):
        board = Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3, 13,  9],
            [ 7,  0,  6,  4],
        ])
        assert board.is_sorted() is False

        board = Board([
            [ 1,  2,  3,  4],
            [ 5,  6,  7,  8],
            [ 9, 10, 11, 12],
            [13,  0, 14,  0],
        ])
        assert board.is_sorted() is False

        board = Board([
            [ 1,  2,  3,  4],
            [ 5,  6,  7,  8],
            [ 9, 10, 11, 12],
            [13, 14,  0,  0],
        ])
        assert board.is_sorted() is True

    def test__move_chip(self, board: Board):
        assert board._data[3][0] == 7
        assert board._data[3][1] == 0
        board.move_chip(Coords(0, 3), Coords(1, 3))
        assert board._data[3][0] == 0
        assert board._data[3][1] == 7

    def test__get_available_moves(self):
        board = Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3, 13,  9],
            [ 7,  0,  6,  4],
        ])
        assert sorted(board.get_available_moves(), key=lambda a: (a.from_.x, a.from_.y, a.to.x, a.to.y)) \
            == [
                   Move(Coords(x=0, y=3), Coords(x=1, y=3)),
                   Move(Coords(x=1, y=1), Coords(x=2, y=1)),
                   Move(Coords(x=1, y=2), Coords(x=1, y=3)),
                   Move(Coords(x=2, y=0), Coords(x=2, y=1)),
                   Move(Coords(x=2, y=2), Coords(x=2, y=1)),
                   Move(Coords(x=2, y=3), Coords(x=1, y=3)),
                   Move(Coords(x=3, y=1), Coords(x=2, y=1))
               ]

        board = Board([
            [ 1,  5,  2, 12],
            [11,  8,  0, 10],
            [14,  3,  0,  9],
            [ 7, 13,  6,  4],
        ])
        assert sorted(board.get_available_moves(), key=lambda a: (a.from_.x, a.from_.y, a.to.x, a.to.y)) \
            == [
                   Move(Coords(x=1, y=1), Coords(x=2, y=1)),
                   Move(Coords(x=1, y=2), Coords(x=2, y=2)),
                   Move(Coords(x=2, y=0), Coords(x=2, y=1)),
                   Move(Coords(x=2, y=3), Coords(x=2, y=2)),
                   Move(Coords(x=3, y=1), Coords(x=2, y=1)),
                   Move(Coords(x=3, y=2), Coords(x=2, y=2)),
               ]
