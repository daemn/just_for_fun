from typing import TypeVar, Iterable
import dataclasses
import enum
import pytest

T = TypeVar('T')


def nested_iterator(*args: T) -> T:
    args: list[Iterable]
    if len(args) > 1:
        for item in args[0]:
            for other_items in nested_iterator(*args[1:]):
                yield item, *other_items
    else:
        for item in args[0]:
            yield (item, )


@dataclasses.dataclass(frozen=True)
class Coords:
    x: int
    y: int

    def __post_init__(self):
        if not (0 <= self.x <= 3) or not (0 <= self.y <= 3):
            raise ValueError('Coords are out or range.')

    def __lt__(self, other):
        return self.x < other.x or self.y < other.y


class Direction(enum.Enum):
    UP = '^'
    DOWN = 'v'
    LEFT = '<'
    RIGHT = '>'
    NOWHERE = ''


@dataclasses.dataclass(frozen=True)
class Move:
    from_: Coords
    to: Coords

    def __post_init__(self):
        if self.from_.x == self.to.x and self.from_.y == self.to.y:
            raise ValueError('Moving nowhere.')
        if abs(self.to.x - self.from_.x) > 1 or abs(self.to.y - self.from_.y) > 1:
            raise ValueError('Move must be one cell size.')
        if self.from_.x != self.to.x and self.from_.y != self.to.y:
            raise ValueError('Move must be horizontal or vertical.')

    def __lt__(self, other):
        return self.from_ < other.from_ or self.to < other.to

    def direction(self) -> Direction:
        if self.from_.x == self.to.x:
            return [Direction.UP, Direction.DOWN][self.from_.y < self.to.y]
        else:
            return [Direction.LEFT, Direction.RIGHT][self.from_.x < self.to.x]


def determine_sign(x):
    """Returns sign of input number: -x -> -1, 0 -> 0, +x -> +1."""
    return (x > 0) - (x < 0)


def read_board_data_from_file(filename: str) -> list[list[int]]:
    """File must contain lines with comma separated numbers. One line is one board row."""
    with open(filename, 'r') as file:
        return [
            [int(x) for x in a.rstrip('\n').split(',')]
            for a in file.readlines()
        ]


def save_history_to_file(history: list[Move], filename: str):
    with open(filename, 'w') as file:
        for move in history:
            file.write(f'{move.from_.x},{move.from_.y},{move.to.x},{move.to.y}\n')


def get_home_cell(number: int) -> Coords:
    """Returns coords of cell where chip should be."""
    y, x = divmod(number - 1, 4)
    return Coords(x, y)


def get_cell_owner(cell: Coords) -> int:
    """Returns the chip that should be in this cell."""
    result: int = cell.y * 4 + cell.x + 1
    return result if result < 15 else 0


class Board:
    history: list[Move]
    _data: list[list[int]]

    def __init__(self, board_data: list[list[int]]):
        self.history = []
        self._data = board_data
        if not self.is_board_valid():
            raise ValueError('Invalid board data.')

    def pprint(self, move: Move = None, fill_char: str = '░'):
        print(fill_char * (3 * 4 + 1))  # top frame line

        out_matrix: list[list[str]] = []
        for row in self._data:
            out_row: list[str] = []
            for chip in row:
                out_row.append(fill_char)
                out_row.append(f'{chip:2}' if chip else fill_char * 2)
            out_row.append(fill_char)
            out_matrix.append(out_row)

            out_row = []
            for _ in range(4):
                out_row.append(fill_char)
                out_row.append(fill_char * 2)
            out_row.append(fill_char)
            out_matrix.append(out_row)

        move_direction: Direction = move.direction() if move else Direction.NOWHERE
        if move_direction != Direction.NOWHERE:
            if move_direction == Direction.LEFT:
                out_matrix[move.from_.y * 2][move.from_.x * 2] = move_direction.value
            elif move_direction == Direction.RIGHT:
                out_matrix[move.from_.y * 2][move.to.x * 2] = move_direction.value
            elif move_direction == Direction.UP:
                out_matrix[move.to.y * 2 + 1][move.from_.x * 2 + 1] = f'{fill_char}{move_direction.value}'
            elif move_direction == Direction.DOWN:
                out_matrix[move.from_.y * 2 + 1][move.from_.x * 2 + 1] = f'{fill_char}{move_direction.value}'

        for out_row in out_matrix:
            print(''.join(out_row))

    def get_chip(self, cell: Coords) -> int:
        return self._data[cell.y][cell.x]

    def get_cell(self, chip: int) -> Coords:
        flat_board: list[int] = list(sum(self._data, []))
        index: int = flat_board.index(chip)
        y, x = divmod(index, 4)
        return Coords(x, y)

    def move_chip(self, move: Move):
        if self._data[move.to.y][move.to.x] != 0:
            raise RuntimeError(f'Try to move chip from {move.from_} to non free cell {move.to}.')
        if self._data[move.from_.y][move.from_.x] == 0:
            raise RuntimeError(f'Try to move chip from free cell {move.from_}.')
        self.history.append(move)
        self._data[move.to.y][move.to.x] = self._data[move.from_.y][move.from_.x]
        self._data[move.from_.y][move.from_.x] = 0

    def is_board_valid(self) -> bool:
        chips: list[int] = sorted(list(sum(self._data, [])))
        return chips == [0, 0] + [a for a in range(1, 15)]

    def is_board_solved(self) -> bool:
        chips: list[int] = list(sum(self._data, []))
        return chips == [a for a in range(1, 15)] + [0, 0]

    def undo_one_move(self):
        move: Move = self.history.pop(-1)
        self._data[move.from_.y][move.from_.x] = self._data[move.to.y][move.to.x]
        self._data[move.to.y][move.to.x] = 0

    def get_free_cells(self) -> list[Coords]:
        result: list[Coords] = []
        for y in range(4):
            for x in range(4):
                if self._data[y][x] == 0:
                    result.append(Coords(x, y))
        return result

    def get_available_moves(self) -> list[Move]:
        moves: list[Move] = []
        free_cells: list[Coords] = self.get_free_cells()
        cell: Coords
        for cell in free_cells:
            for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
                if not (0 <= cell.x + dx <= 3) or not (0 <= cell.y + dy <= 3):
                    continue
                cell_from = Coords(cell.x + dx, cell.y + dy)
                if cell_from in free_cells:
                    continue
                moves.append(Move(cell_from, cell))
        return moves


class TestAllTools:
    """Test class for pytest"""

    def test__determine_sign(self):
        assert determine_sign(0) == 0
        assert determine_sign(3) == 1
        assert determine_sign(-5) == -1

    def test__nested_iterator(self):
        assert list(nested_iterator([1, 2, 3], [4, 5], ['a', 'b'])) == [
            (1, 4, 'a'), (1, 4, 'b'),
            (1, 5, 'a'), (1, 5, 'b'),
            (2, 4, 'a'), (2, 4, 'b'),
            (2, 5, 'a'), (2, 5, 'b'),
            (3, 4, 'a'), (3, 4, 'b'),
            (3, 5, 'a'), (3, 5, 'b'),
        ]

    def test__move_direction(self):
        assert Move(Coords(1, 1), Coords(2, 1)).direction() == Direction.RIGHT
        assert Move(Coords(1, 1), Coords(1, 2)).direction() == Direction.DOWN

    def test__home_cell(self):
        assert get_home_cell(1) == Coords(x=0, y=0)
        assert get_home_cell(9) == Coords(x=0, y=2)
        assert get_home_cell(14) == Coords(x=1, y=3)

    def test__cell_owner(self):
        assert get_cell_owner(Coords(x=0, y=0)) == 1
        assert get_cell_owner(Coords(x=3, y=0)) == 4
        assert get_cell_owner(Coords(x=1, y=3)) == 14
        assert get_cell_owner(Coords(x=3, y=3)) == 0

    @pytest.fixture
    def board(self) -> Board:
        return Board([
            [ 1, 5, 2, 12],
            [11, 8, 0, 10],
            [14, 3, 4,  9],
            [ 7, 0, 6, 13],
        ])

    def test__get_cell(self, board: Board):
        assert board.get_cell(1) == Coords(x=0, y=0)
        assert board.get_cell(3) == Coords(x=1, y=2)
        assert board.get_cell(13) == Coords(x=3, y=3)

    def test__move_chip__good(self, board: Board):
        assert board.get_chip(Coords(1, 2)) == 3
        assert board.get_chip(Coords(1, 3)) == 0
        board.move_chip(Move(Coords(1, 2), Coords(1, 3)))
        assert board.get_chip(Coords(1, 2)) == 0
        assert board.get_chip(Coords(1, 3)) == 3

    def test__move_chip__bad(self, board: Board):
        with pytest.raises(RuntimeError):
            board.move_chip(Move(Coords(1, 1), Coords(1, 2)))

        with pytest.raises(RuntimeError):
            board.move_chip(Move(Coords(2, 1), Coords(3, 1)))

    def test__is_board_valid(self, board: Board):
        assert board.is_board_valid() is True
        with pytest.raises(ValueError):
            Board([
                [0,  5, 2, 12],
                [11, 8, 0, 10],
                [14, 3, 4,  9],
                [7,  0, 6, 13],
            ])

    def test__is_board_solved(self, board: Board):
        assert board.is_board_solved() is False

        board = Board([
            [1,   2,  3,  4],
            [5,   6,  7,  8],
            [9,  10, 11, 12],
            [13, 14,  0,  0],
        ])
        assert board.is_board_solved() is True

    def test__undo_one_move(self, board: Board):
        board.move_chip(Move(Coords(1, 2), Coords(1, 3)))
        assert board.history == [Move(Coords(1, 2), Coords(1, 3))]
        board.move_chip(Move(Coords(2, 2), Coords(1, 2)))
        assert board.history == [Move(Coords(1, 2), Coords(1, 3)), Move(Coords(2, 2), Coords(1, 2))]
        board.undo_one_move()
        assert board.history == [Move(Coords(1, 2), Coords(1, 3))]
        assert board._data == [
            [1,  5, 2, 12],
            [11, 8, 0, 10],
            [14, 0, 4,  9],
            [7,  3, 6, 13],
        ]

    def test__get_free_cells(self, board: Board):
        assert board.get_free_cells() == [Coords(2, 1), Coords(1, 3)]

    def test__get_available_moves(self, board: Board):
        assert sorted(board.get_available_moves()) == [
            Move(from_=Coords(x=1, y=2), to=Coords(x=1, y=3)),
            Move(from_=Coords(x=0, y=3), to=Coords(x=1, y=3)),
            Move(from_=Coords(x=2, y=3), to=Coords(x=1, y=3)),
            Move(from_=Coords(x=2, y=0), to=Coords(x=2, y=1)),
            Move(from_=Coords(x=1, y=1), to=Coords(x=2, y=1)),
            Move(from_=Coords(x=2, y=2), to=Coords(x=2, y=1)),
            Move(from_=Coords(x=3, y=1), to=Coords(x=2, y=1))
        ]

        board = Board([
            [1,   2,  3,  4],
            [5,   6,  7,  8],
            [9,  10, 11, 12],
            [13, 14,  0,  0],
        ])
        assert sorted(board.get_available_moves()) == [
            Move(from_=Coords(x=2, y=2), to=Coords(x=2, y=3)),
            Move(from_=Coords(x=3, y=2), to=Coords(x=3, y=3)),
            Move(from_=Coords(x=1, y=3), to=Coords(x=2, y=3))
        ]
