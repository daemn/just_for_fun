# encoding: UTF-8

# There are list of integer numbers: X1, X2, ..., Xn. Put between them "+" and "-" signs, so that the value
# of the resulting expression is equal to the given integer S.
#
# 2 <= n <= 24;
# 0 <= Xi <= 50 000 000;
# -1 000 000 000 <= S <= 1 000 000 000;
#
# If there is more than one solution, return any one of them.


def calc(numbers: list[int], target: int) -> str:
    sign_quantity: int = len(numbers) - 1
    for int_bits in range(2 ** sign_quantity):
        str_bits: str = '{:0{}b}'.format(int_bits, sign_quantity)
        signs: list[int] = [1] + [1 if a == '0' else -1 for a in str_bits]
        signed_numbers: list[int] = [a * b for a, b in zip(signs, numbers)]
        if sum(signed_numbers) == target:
            expression: str = ''.join(map(lambda a: str(a) if a < 0 else f'+{a}', signed_numbers)).lstrip('+')
            return f'{expression}={target}'

    return 'No solution'


if __name__ == '__main__':
    assert calc([5, 2, 9], 12) == '5-2+9=12'
    assert calc([3, 45, 2, 67, 5, 1, 1, 2, 2], 16) == '3-45+2+67-5-1-1-2-2=16'
    assert calc([30, 45, 67], 49) == 'No solution'
