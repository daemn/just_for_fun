# encoding: UTF-8

__doc__ = """Bit reverse of one byte."""

# Process time:
# reverse_byte_static |===
# reverse_byte_str    |====
# reverse_byte_if     |========
# reverse_byte_shift  |========
# reverse_byte_divmul |==================


def reverse_byte_static(num):
    """Static conditions with bit mask."""
    result = 0
    if num & 128: result += 1  # It is faster than "result |= 1"
    if num & 64:  result += 2
    if num & 32:  result += 4
    if num & 16:  result += 8
    if num & 8:   result += 16
    if num & 4:   result += 32
    if num & 2:   result += 64
    if num & 1:   result += 128
    return result


def reverse_byte_str(num):
    """String functions."""
    string = '{:08b}'.format(num)
    result = int(string[::-1], 2)
    return result


def reverse_byte_if(num):
    """Condition with bit mask."""
    result = 0
    mask = 128
    add = 1
    for i in range(8):
        if num & mask:
            result += add
        mask //= 2  # It is faster than "num >>= 1"
        add *= 2
    return result


def reverse_byte_shift(num):
    """Bit operators."""
    result = 0
    for i in range(7, -1, -1):
        result += (num & 1) << i
        num //= 2
    return result


def reverse_byte_divmul(num):
    """Arithmetic operators."""
    div = 128
    result = 0
    for i in range(8):
        mul = 2 ** i
        result += num // div * mul
        num %= div
        div //= 2
    return result
