from dataclasses import dataclass

__doc__ = """The king walked for a long time on the infinite chessboard. We know the sequence of his moves (up, down,
up-right, e.t.c). Write a program that could check the king was in the same cell twice.
"""


@dataclass(frozen=True)
class Cell:
    x: int
    y: int


def complicated(moves: list[str]) -> bool:
    actions = {
        'up': lambda a: Cell(a.x, a.y + 1),
        'down': lambda a: Cell(a.x, a.y - 1),
        'left': lambda a: Cell(a.x - 1, a.y),
        'right': lambda a: Cell(a.x + 1, a.y),
    }
    cell = Cell(0, 0)
    track: list[Cell] = [cell]
    for move_ in moves:
        if '-' in move_:
            for move__ in move_.split('-'):
                cell = actions[move__](cell)
        else:
            cell = actions[move_](cell)
        track.append(cell)

    return len(set(track)) != len(track)


def simple(moves: list[str]) -> bool:
    actions = {
        'up': lambda a: Cell(a.x, a.y + 1),
        'down': lambda a: Cell(a.x, a.y - 1),
        'right': lambda a: Cell(a.x + 1, a.y),
        'left': lambda a: Cell(a.x - 1, a.y),
        'up-right': lambda a: Cell(a.x + 1, a.y + 1),
        'right-up': lambda a: Cell(a.x + 1, a.y + 1),
        'down-right': lambda a: Cell(a.x + 1, a.y - 1),
        'right-down': lambda a: Cell(a.x + 1, a.y - 1),
        'down-left': lambda a: Cell(a.x - 1, a.y - 1),
        'left-down': lambda a: Cell(a.x - 1, a.y - 1),
        'up-left': lambda a: Cell(a.x - 1, a.y + 1),
        'left-up': lambda a: Cell(a.x - 1, a.y + 1),
    }
    cell: Cell = Cell(0, 0)
    track: list[Cell] = [cell]
    for move in moves:
        cell = actions[move](cell)
        if cell in track:
            return True
        track.append(cell)
    return False


if __name__ == '__main__':
    assert complicated(['up', 'up-right', 'down']) is False
    assert complicated(['up', 'right-up', 'down', 'down-left']) is True

    assert simple(['up', 'up-right', 'down']) is False
    assert simple(['up', 'right-up', 'down', 'down-left']) is True
