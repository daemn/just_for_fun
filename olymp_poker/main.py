#!/usr/bin/env python3
# encoding: UTF-8
import enum
import typing as t

# There are 5 integer numbers. 1 <= N <= 13.
# - if 5 numbers is equal, then return "Impossible";
# - if 4 numbers is equal, then return "Four of a Kind";
# - if 3 numbers is equal and 2 numbers is equal, then return "Full House";
# - if 5 numbers in a row, then return "Straight";
# - if 3 numbers is equal, then return "Three of a Kind";
# - if 2 numbers is equal and 2 numbers is equal, then return "Two Pairs";
# - if 2 numbers is equal, then return "One Pair";
# - else return "Nothing";


class Result(enum.Enum):
    impossible: str = 'Impossible'
    four: str = 'Four of a Kind'
    full: str = 'Full House'
    straight: str = 'Straight'
    three: str = 'Three of a Kind'
    two_pairs: str = 'Two Pairs'
    one_pair: str = 'One Pair'
    nothing: str = 'Nothing'


def longest_sequence(numbers: t.Iterable) -> int:
    sorted_list: list[int] = sorted(set(numbers))
    answer: int = 1
    sequence: int = 1
    for i in range(1, len(list(sorted_list))):
        if sorted_list[i] - sorted_list[i - 1] == 1:
            sequence += 1
            if sequence > answer:
                answer = sequence
        else:
            sequence = 0
    return answer


def check_cards(cards: tuple) -> Result:
    calc: dict[int, int] = dict(map(lambda a: (a, cards.count(a)), set(cards)))
    if 5 in calc.values():
        return Result.impossible
    elif 4 in calc.values():
        return Result.four
    elif 3 in calc.values() and 2 in calc.values():
        return Result.full
    elif longest_sequence(cards) == 5:
        return Result.straight
    elif 3 in calc.values():
        return Result.three
    elif len(list(filter(lambda a: a == 2, calc.values()))) == 2:
        return Result.two_pairs
    elif 2 in calc.values():
        return Result.one_pair
    else:
        return Result.nothing


if __name__ == '__main__':
    assert check_cards((4, 5, 6, 6, 6, 1, 6, 6)) == Result.impossible
    assert check_cards((4, 6, 1, 6, 6, 6)) == Result.four
    assert check_cards((4, 4, 1, 2, 2, 2)) == Result.full
    assert check_cards((4, 4, 1, 2, 5, 3)) == Result.straight
    assert check_cards((4, 7, 2, 1, 2, 2)) == Result.three
    assert check_cards((4, 1, 2, 1, 3, 2)) == Result.two_pairs
    assert check_cards((4, 1, 2, 1, 3, 8)) == Result.one_pair
    assert check_cards((4, 5, 6, 8, 3)) == Result.nothing
