#!/usr/bin/env python3
# encoding: UTF-8
import typing as t
from copy import copy
import time

# There is a square (NxN) matrix with numbers from 0 to 9. We need to find the path from upper left cell (1, 1) to
# lower right cell (N, N). The sum of numbers on the path must be minimal. Only steps down and to the right are allowed.


def print_matrix(matrix: list[list], path: list[str] = []):
    tmp_path: list[str] = copy(path)
    x: int = 0
    out_line: list[str]
    for matrix_line in matrix:

        out_line = []
        for a in matrix_line:
            out_line.append(str(a))
            out_line.append(' ')
        out_line.pop(-1)

        while tmp_path and tmp_path[0] == 'right':
            out_line[x * 2 + 1] = '-'
            tmp_path.pop(0)
            x += 1

        print(''.join(out_line))

        out_line = [' ' for _ in range(len(matrix_line) * 2 - 1)]
        if tmp_path:
            out_line[x * 2] = '|'
            tmp_path.pop(0)

        print(''.join(out_line))

    print('Path cost is', path_cost(matrix, path))


def path_cost(matrix: list[list], path: list[str]) -> int:
    x: int = 0
    y: int = 0
    cost: int = 0
    for step in path:
        cost += matrix[y][x]
        if step == 'right':
            x += 1
        else:
            y += 1
    return cost


def full_selection_of_options(matrix: list[list]) -> list[str]:

    def solutions(path: list[str] = []) -> t.Iterator:
        if len(path) == len(matrix) * 2 - 2:
            yield path
        if path.count('right') < (len(matrix) - 1):
            yield from solutions(path + ['right'])
        if path.count('down') < (len(matrix) - 1):
            yield from solutions(path + ['down'])

    cheapest_path: list[str] = []
    lowest_path_cost: int = 9 * len(matrix) * 2
    for path in solutions():
        cost: int = path_cost(matrix, path)
        if cost < lowest_path_cost:
            lowest_path_cost = cost
            cheapest_path = path

    return cheapest_path


def iter_method(matrix: list[list]) -> list[str]:
    """It draws a diagonal path and iteratively improves it."""
    path: list[str] = sum([['right', 'down'] for _ in range(len(matrix) - 1)], [])

    edited: bool = True
    while edited:
        edited = False
        x, y = 0, 0
        for i in range(len(path) - 1):

            if (path[i], path[i + 1]) == ('right', 'down') and matrix[y + 1][x] < matrix[y][x + 1]:
                path[i], path[i + 1] = 'down', 'right'
                edited = True
            elif (path[i], path[i + 1]) == ('down', 'right') and matrix[y][x + 1] < matrix[y + 1][x]:
                path[i], path[i + 1] = 'right', 'down'
                edited = True

            if path[i] == 'right':
                x += 1
            else:
                y += 1

    return path


if __name__ == '__main__':
    matrix = [
        [5, 5, 5, 4, 3, 3, 3, 3, 3, 6],
        [3, 5, 5, 5, 5, 4, 4, 5, 5, 5],
        [2, 5, 6, 6, 5, 5, 5, 6, 5, 5],
        [3, 6, 7, 8, 6, 6, 5, 6, 8, 5],
        [0, 6, 8, 9, 5, 4, 5, 6, 9, 5],
        [0, 6, 7, 8, 7, 3, 5, 5, 9, 4],
        [0, 6, 7, 6, 6, 4, 5, 5, 9, 3],
        [1, 5, 6, 6, 6, 5, 5, 5, 5, 3],
        [3, 5, 6, 6, 6, 5, 5, 5, 5, 4],
        [6, 7, 7, 8, 9, 7, 5, 5, 5, 5],
    ]
    for f in full_selection_of_options, iter_method:
        print(f'--- {f.__name__} ---')
        path: list[str] = f(matrix)
        print_matrix(matrix, path)
        print()
