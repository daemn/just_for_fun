# encoding: UTF-8
from random import shuffle
import pickle
from hashlib import sha256
from typing import Any, List
from dataclasses import dataclass

__doc__ = """Top items in a stream data. Explaining: https://youtu.be/eVDWMRPsjo4"""


@dataclass
class ItemInfo:
    item: Any
    count: int
    exactly: bool


class SpaceSaving:
    """Define the most frequently occurring items in a stream."""

    __slots__ = ('array_size', 'array')
    array_size: int
    array: dict

    def __init__(self, size_of_top_list: int) -> None:
        """
        :param size_of_top_list: How many top items needed.
        """
        self.array_size = size_of_top_list
        self.array = {}

    def _increase_exists_key(self, key) -> None:
        self.array[key].count += 1

    def _insert_not_exists_key(self, key, item) -> None:
        start_count: int = 1
        exactly_flag: bool = True
        if len(self.array) >= self.array_size:
            exactly_flag: bool = False
            fired_key: bytes = b''
            fired_count: int = 0
            for item_key, item_info in self.array.items():
                if item_info.count < fired_count or not fired_count:
                    fired_key, fired_count = item_key, item_info.count
            start_count = fired_count
            del self.array[fired_key]
        self.array[key] = ItemInfo(item, start_count, exactly_flag)

    def count_item(self, item: Any) -> None:
        """Count this item."""
        item_hash: bytes = sha256(pickle.dumps(item)).digest()
        if item_hash in self.array:
            self._increase_exists_key(item_hash)
        else:
            self._insert_not_exists_key(item_hash, item)

    def __repr__(self) -> str:
        return (
            f'<{self.__class__.__name__}'
            f', array_size: {self.array_size}>'
        )


class TestSpaceSaving:

    def test__count(self) -> None:
        ss: SpaceSaving = SpaceSaving(3)

        items: List[str] = list('a' * 10 + 'b' * 30 + 'c' * 100 + 'd' * 5 + 'e' * 6)
        shuffle(items)
        for item in items:
            ss.count_item(item)

        assert len(ss.array) == 3

        for item_info in ss.array.values():
            if item_info.item == 'a':
                assert 8 < item_info.count < 12
            elif item_info.item == 'b':
                assert 25 < item_info.count < 35
            elif item_info.item == 'c':
                assert 90 < item_info.count < 110
