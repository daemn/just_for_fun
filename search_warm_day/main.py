# encoding: UTF-8
from dataclasses import dataclass

# There is a list of temperature values. Find the number of days until warming occurs for each day. Set zero for the
# last day.


def search(term_log: list) -> list:
    """One read from right to left with stack."""

    @dataclass
    class StackItem:
        index: int
        value: int

    result: list[int] = []
    stack: list[StackItem] = []
    for index in range(len(term_log) - 1, -1, -1):
        value: int = term_log[index]

        while stack and value >= stack[-1].value:
            stack.pop()

        if stack:
            result: list = [stack[-1].index - index] + result
        else:
            result: list = [0] + result
        stack.append(StackItem(index, value))

    return result


def search2(term_log: list) -> list:
    """Read from left to right."""
    result: list[int] = []

    for index in range(len(term_log) - 1):
        for index2 in range(index + 1, len(term_log)):
            if term_log[index] < term_log[index2]:
                result.append(index2 - index)
                break
        else:
            result.append(0)

    result.append(0)
    return result


if __name__ == '__main__':
    source_data: list[int] = [13, 12, 15, 11, 9, 12, 16]
    valid_result: list[int] = [2, 1, 4, 2, 1, 1, 0]

    for f in search, search2:
        assert f(source_data) == valid_result
