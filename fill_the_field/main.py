# encoding: UTF-8

from time import sleep
from collections import defaultdict
import sys


__doc__ = """
Fill the defined field.

# - wall
S - start

Execute program in terminal.
"""

field = [
    '############################################################',
    '#      #           #                                       #',
    '#      #           #                                       #',
    '#      #           #                          #            #',
    '#      #           #                          #            #',
    '#      #           #                      ############     #',
    '#                  #                                 #     #',
    '#                  #                                 #     #',
    '#                  #    ################             #     #',
    '#                  #                   #             #     #',
    '#      ########                        #             #     #',
    '#           #   ####                   #           #####   #',
    '#       S   #                          #             #     #',
    '#           #                          #                   #',
    '#           #                          #                   #',
    '#           #                          #                   #',
    '#           #                                              #',
    '############################################################',
]


palette: list = ['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']


def text_color_to_code(name: str) -> int:
    """Translate color name to color code for text."""
    return 30 + palette.index(name)


def background_color_to_code(name: str) -> int:
    """Translate color name to color code for background."""
    return 40 + palette.index(name)


def cursor_to(col: int, row: int) -> None:
    """Move cursor to the specified position.

    :param col: Column.
    :param row: Row.
    """
    sys.stdout.write(f'\033[{row};{col}H')


class Style:
    """Complex terminal output style.
    Color palette noted in attribute of "palette" module.

    Attributes:
    text_color          - Text color.
    background_color    - Background color.
    bold                - Bold text.
    italic              - Italic text.
    underline           - Underlined text.
    flash               - Flashed text.
    inversion           - Inverted colors.
    """

    text_color: str
    background_color: str
    bold: bool
    italic: bool
    underline: bool
    flash: bool
    inversion: bool

    def __init__(self) -> None:
        self.text_color = 'white'
        self.background_color = 'black'
        self.bold = False
        self.italic = False
        self.underline = False
        self.flash = False
        self.inversion = False

    def compile_ansi_code(self) -> str:
        res: str = '\033[0m'
        res += f'\033[{text_color_to_code(self.text_color)}m'
        res += f'\033[{background_color_to_code(self.background_color)}m'
        if self.bold:
            res += '\033[1m'
        if self.italic:
            res += '\033[3m'
        if self.underline:
            res += '\033[4m'
        if self.flash:
            res += '\033[5m'
        if self.inversion:
            res += '\033[7m'
        return res


def flush() -> None:
    """Flush the screen buffer."""
    sys.stdout.flush()


def write(text: str) -> None:
    """Write text to the stdout.

    :param text: Output text.
    """
    sys.stdout.write(text)


def print_at(col: int, row: int, text: str, style: Style = None) -> None:
    """Output text to the terminal at the specified position.

    :param col: Output column.
    :param row: Output row.
    :param text: Output text.
    :param style: Style object.
    """
    if style is not None:
        write(style.compile_ansi_code())
    cursor_to(col, row)
    write(text)
    flush()


if __name__ == '__main__':
    data = defaultdict(lambda: ' ')
    stack = []
    for y, row in enumerate(field):
        for x, char in enumerate(row):
            if char == '#':
                data[(x, y)] = char
                print_at(x + 1, y + 1, '#')
            elif char == 'S':
                data[(x, y)] = char
                stack = [(x, y)]

    while stack:
        sleep(0.03)
        cell_x, cell_y = stack.pop(0)
        data[(cell_x, cell_y)] = '-'
        print_at(cell_x + 1, cell_y + 1, '-')
        for check_x, check_y in (
            (cell_x + 1, cell_y),
            (cell_x - 1, cell_y),
            (cell_x, cell_y + 1),
            (cell_x, cell_y - 1),
        ):
            if data[(check_x, check_y)] == ' ':
                stack.append((check_x, check_y))
                data[(check_x, check_y)] = '+'
                print_at(check_x + 1, check_y + 1, '+')
