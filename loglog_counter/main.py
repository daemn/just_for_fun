# encoding: UTF-8
import pickle
from hashlib import blake2b
from typing import Any, List

__doc__ = """How many different items are in a stream data. Explaining: https://youtu.be/eVDWMRPsjo4"""


class BucketHash:
    """Calculate a hash of an item."""

    __slots__ = ('hash_bytes', 'hash_bits')
    hash_bytes: bytes   # The hash as bytes.
    hash_bits: str      # The hash as bits.

    def __init__(self, item: Any, hash_size: int) -> None:
        """
        :param item: An incoming item.
        :param hash_size: The size of the hash in bytes.
        """
        self.hash_bytes = blake2b(pickle.dumps(item), digest_size=hash_size).digest()
        self.hash_bits = ''.join(['{:08b}'.format(num) for num in self.hash_bytes])

    def get_index(self, index_bits_number: int) -> int:
        """Return the bucket index."""
        return int(self.hash_bits[:index_bits_number], 2)

    def get_rank(self) -> int:
        """Return the hash rank."""
        return len(self.hash_bits) - self.hash_bits.rfind('1')


class LogLogCounter:
    """Count unique items in a stream."""

    __slots__ = ('buckets', 'hash_size', 'index_size')
    buckets: List[int]  # An array of buckets.
    hash_size: int      # A size of a generated item hash.
    index_size: int     # A number of first bits of the item hash, used as a bucket index.

    def __init__(self, possible_uniq_items: int, index_size: int) -> None:
        """
        :param possible_uniq_items: The number of possible uniq items.
        :param index_size: The size of bucket index in bits.
        """
        self.hash_size = 1
        while pow(256, self.hash_size) < possible_uniq_items:
            self.hash_size += 1

        self.index_size = index_size
        if self.index_size > self.hash_size * 8:
            self.hash_size = self.index_size // 8 + 1

        self.buckets = [0] * pow(2, self.index_size)

    def remember_item(self, item: Any) -> None:
        """Remember an item in the array."""
        item_hash: BucketHash = BucketHash(item, self.hash_size)
        index: int = item_hash.get_index(self.index_size)
        self.buckets[index] = max(self.buckets[index], item_hash.get_rank())

    def number_of_uniq_items(self, estimation_factor: float = 0.39) -> int:
        """Return number of unique items."""
        buckets_number: int = len(self.buckets)
        average_rank: float = sum(self.buckets, 0) / buckets_number
        return int(estimation_factor * buckets_number * pow(2, average_rank))

    def __repr__(self) -> str:
        return (
            f'<{self.__class__.__name__}'
            f', hash_size: {self.hash_size}'
            f', index_size: {self.index_size}'
            f', buckets: {self.buckets}>'
        )


class TestBucketHash:

    def test__hash_bytes(self) -> None:
        bh: BucketHash = BucketHash('foo', 3)
        assert bh.hash_bytes == b'\x8b\xebV'

        bh: BucketHash = BucketHash({'foo': 'bar'}, 5)
        assert bh.hash_bytes == b'\x83[\xaa\xbbH'

    def test__hash_bits(self) -> None:
        bh: BucketHash = BucketHash('foo', 3)
        assert bh.hash_bits == '100010111110101101010110'

        bh: BucketHash = BucketHash({'foo': 'bar'}, 5)
        assert bh.hash_bits == '1000001101011011101010101011101101001000'

    def test__get_index(self) -> None:
        bh: BucketHash = BucketHash('foo', 3)   # 100010111110101101010110
        assert bh.get_index(1) == 1             # 1...
        assert bh.get_index(3) == 4             # 100...
        assert bh.get_index(5) == 17            # 10001...

    def test__get_rank(self) -> None:
        bh: BucketHash = BucketHash('foo', 3)   # 100010111110101101010110
        assert bh.get_rank() == 2               # ......................^.

        bh: BucketHash = BucketHash('bar', 3)   # 100110101001100001001100
        assert bh.get_rank() == 3               # .....................^..

        bh: BucketHash = BucketHash('zoo', 3)   # 000110100111100001000000
        assert bh.get_rank() == 7               # .................^......


class TestLogLogCounter:

    def test__init(self) -> None:
        lc: LogLogCounter = LogLogCounter(100, 5)
        assert lc.hash_size == 1
        assert lc.index_size == 5

        lc: LogLogCounter = LogLogCounter(10000, 8)
        assert lc.hash_size == 2
        assert lc.index_size == 8

    def test__init__big_index(self) -> None:
        lc: LogLogCounter = LogLogCounter(100, 10)
        assert lc.hash_size == 2
        assert lc.index_size == 10

        lc: LogLogCounter = LogLogCounter(100, 17)
        assert lc.hash_size == 3
        assert lc.index_size == 17

    def test__remember_item(self) -> None:
        lc: LogLogCounter = LogLogCounter(100, 3)

        lc.remember_item('foo')     # 00000101
        assert list(lc.buckets) == [1, 0, 0, 0, 0, 0, 0, 0]

        lc.remember_item('4')       # 00011110
        assert list(lc.buckets) == [2, 0, 0, 0, 0, 0, 0, 0]

        lc.remember_item('bar')     # 10011000
        assert list(lc.buckets) == [2, 0, 0, 0, 4, 0, 0, 0]

        lc.remember_item('Mozgovynosyashki')    # 10001101
        assert list(lc.buckets) == [2, 0, 0, 0, 4, 0, 0, 0]

    def test__number_of_uniq_items(self) -> None:
        lc: LogLogCounter = LogLogCounter(65000, 10)
        for item in range(2000):
            lc.remember_item(item)
        for item in range(1000, 4000):
            lc.remember_item(item)
        assert 3000 < lc.number_of_uniq_items() < 5000
