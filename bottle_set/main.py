# encoding: UTF-8
from math import ceil

__doc__ = """Find the optimal set of bottle packaging.
- The number of bottles will be greater than or equal to the target.
- The number of packages in a set is minimal.

E.g.:
>>> compile_packs(140, [3, 12, 31])
[31, 31, 12, 12, 12, 12, 12, 12, 3, 3]
"""


def compile_packs(target_number: int, pack_set: list[int]) -> list[int]:
    """Find the optimal set of bottle packaging.

    :param target_number: Number of bottles that you need.
    :param pack_set: Set of available bottle packaging.
    :return: Optimal set of bottle packaging.
    """
    # Separate the biggest pack and sub packs for recursive call.
    sub_packs: list[int] = list(sorted(pack_set))
    biggest_pack: int = sub_packs.pop(-1)

    # Fill the current set with the biggest pack.
    current_set: list[int] = [biggest_pack] * ceil(target_number / biggest_pack)
    if not sub_packs:
        return current_set

    optimal_set: list[int] = current_set
    while current_set:
        if sum(optimal_set) == target_number:
            return optimal_set

        # Cut off the last pack from current set. And try to supplement set with smaller packs.
        current_set = current_set[:-1]
        additional_set: list[int] = compile_packs(
            target_number=target_number - sum(current_set), pack_set=sub_packs
        )
        full_current_set: list[int] = current_set + additional_set

        if (sum(full_current_set) - target_number) < (sum(optimal_set) - target_number):
            optimal_set = full_current_set

    return optimal_set


if __name__ == '__main__':
    assert compile_packs(21, [5, 7, 8]) == [8, 8, 5]
    assert compile_packs(22, [5, 7, 8]) == [8, 7, 7]
    assert compile_packs(25, [5, 7, 8]) == [8, 7, 5, 5]
    assert compile_packs(140, [3, 12, 31]) == [31, 31, 12, 12, 12, 12, 12, 12, 3, 3]
    assert compile_packs(14, [3, 12, 31]) == [12, 3]
