# encoding: UTF-8
import typing as t

__doc__ = """Calculate value by chain of object methods.
E.g.:
>>> m = Math()
>>> m.seven().times().five()
35
"""


class Math:
    _expression: str
    _have_operator: bool

    @staticmethod
    def _reg_num(num: str) -> t.Callable:
        def func(self) -> t.Union[int, t.Callable]:
            self._expression += num
            return self._eval_expression()
        return func

    @staticmethod
    def _reg_oper(oper: str) -> t.Callable:
        def func(self) -> t.Callable:
            self._expression += oper
            self._have_operator = True
            return self
        return func

    zero: t.Callable = _reg_num('0')
    one: t.Callable = _reg_num('1')
    two: t.Callable = _reg_num('2')
    three: t.Callable = _reg_num('3')
    four: t.Callable = _reg_num('4')
    five: t.Callable = _reg_num('5')
    six: t.Callable = _reg_num('6')
    seven: t.Callable = _reg_num('7')
    eight: t.Callable = _reg_num('8')
    nine: t.Callable = _reg_num('9')

    times: t.Callable = _reg_oper('*')
    plus: t.Callable = _reg_oper('+')
    minus: t.Callable = _reg_oper('-')
    div: t.Callable = _reg_oper('/')

    def __init__(self):
        self._expression = ''
        self._have_operator = False

    def _eval_expression(self) -> t.Union[int, t.Callable]:
        if self._have_operator:
            result: int = eval(self._expression)
            self._have_operator = False
            self._expression = ''
            return result
        else:
            return self


if __name__ == '__main__':
    m = Math()
    assert m.seven().times().five() == 35
    assert m.nine().div().three() == 3
