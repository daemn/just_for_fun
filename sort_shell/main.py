# encoding: UTF-8
import random
from copy import copy

__doc__ = """Shell sorting algorithm."""


def shell(sequence: list[int]) -> list[int]:
    # Идея метода Шелла состоит в сравнении элементов, стоящих не только рядом, но и на определённом расстоянии друг от
    # друга. Иными словами — это сортировка вставками с предварительными «грубыми» проходами.
    # При сортировке Шелла сначала сравниваются и сортируются между собой значения, стоящие один от другого на некотором
    # расстоянии d. После этого процедура повторяется для некоторых меньших значений d, а завершается сортировка Шелла
    # упорядочиванием элементов при d=1 (то есть обычной сортировкой вставками).

    delta: int = int(len(sequence) // 2)
    while delta > 0:
        for sub_sequence_begin in range(0, delta):
            for waterline in range(sub_sequence_begin + delta, len(sequence), delta):
                index: int = waterline
                while index > sub_sequence_begin and sequence[index] < sequence[index - delta]:
                    sequence[index - delta], sequence[index] = sequence[index], sequence[index - delta]
                    index -= delta
        delta = int(delta // 2)

    return sequence


if __name__ == '__main__':
    random_sequence: list[int] = [random.randint(10, 99) for _ in range(1_000)]
    valid_result: list = sorted(random_sequence)
    assert valid_result == shell(copy(random_sequence))
