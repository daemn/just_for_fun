#!/usr/bin/env python3
# encoding: UTF-8

__doc__ = """Resolve the Hanoy Towers puzzle."""

RING_POSITION = [0, 0, 0]


def draw_towers():
    towers = [0, 0, 0]
    for r in RING_POSITION:
        towers[r] += 1
    print()
    for ring in range(3):
        for tower in range(3):
            if RING_POSITION[ring] == tower:
                print('O ', end='')
            else:
                print('| ', end='')
        print()
    print('=' * 5)
    print()


def who_is_there(ring, tower):
    for r in range(ring, -1, -1):
        if RING_POSITION[r] == tower:
            return r
    return None


def move_ring(ring, dest):
    global RING_POSITION
    source = RING_POSITION[ring]
    while True:
        # pdb.set_trace()
        coverer = who_is_there(ring - 1, source)
        if coverer is None:
            coverer = who_is_there(ring - 1, dest)
        if coverer is None:
            break
        move_ring(coverer, 3 - source - dest)
    RING_POSITION[ring] = dest
    print('move ring {} from {} to {}'.format(ring, source, dest))
    draw_towers()


if __name__ == '__main__':
    for r in range(2, -1, -1):
        move_ring(r, 1)
