# encoding: UTF-8


def flat_fibonacci(number):
    result = 0
    next_ = 1
    for _ in range(number):
        result, next_ = next_, result + next_
    return result


def recurse_fibonacci(number):
    if number <= 1:
        return number
    return recurse_fibonacci(number - 1) + recurse_fibonacci(number - 2)


if __name__ == '__main__':
    for number in [0, 1, 2, 3, 4, 5, 6, 7, 8, 22]:
        assert flat_fibonacci(number) == recurse_fibonacci(number)
