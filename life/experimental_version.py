import time
import enum
from typing import NoReturn

'''Rules:
- Any live cell with fewer than two live neighbours dies.
- Any live cell with more than three live neighbours dies.
- Any dead cell with exactly three live neighbours becomes a live cell.

This app is kind of experiment. It allocate memory only for one field but has a bug because of that.
'''


class CellState(enum.StrEnum):
    DEAD = ' '
    BORN = 'o'
    ALIVE = '@'
    SICK = '+'


class World:
    world_map: list[list[CellState]]
    x_size: int
    y_size: int

    def __init__(self, init_map: list[str] = None):
        self.x_size: int = max(*map(len, init_map)) if init_map else 10
        self.y_size: int = len(init_map) if init_map else 10
        print(f'Init map size: {self.x_size}x{self.y_size}')

        self.world_map = [[CellState.DEAD for _ in range(self.x_size)] for _ in range(self.y_size)]
        if not init_map:
            return
        for y, row in enumerate(init_map):
            for x, cell in enumerate(row):
                if cell != ' ':
                    self.set_cell(x, y, CellState.ALIVE)

    def set_cell(self, x: int, y: int, state: CellState):
        self.world_map[y % self.y_size][x % self.x_size] = state

    def get_cell(self, x: int, y: int) -> CellState:
        return self.world_map[y % self.y_size][x % self.x_size]


class Life:
    world: World
    step_no: int

    def __init__(self, world: World):
        self.world = world
        self.step_no = 0

    def _count_cells_around(self, x: int, y: int) -> int:
        count = 0
        for dx, dy in [(-1, -1), (0, -1), (1, -1), (-1, 0)]:
            cell = self.world.get_cell(x + dx, y + dy)
            count += (cell is CellState.ALIVE or cell is CellState.SICK)
        for dx, dy in [(1, 0), (-1, 1), (0, 1), (1, 1)]:
            cell = self.world.get_cell(x + dx, y + dy)
            count += (cell is CellState.ALIVE or cell is CellState.BORN)
        return count

    def step(self):
        self.step_no += 1
        for y in range(self.world.y_size):
            for x in range(self.world.x_size):
                cell = self.world.get_cell(x, y)
                alive_around = self._count_cells_around(x, y)
                if cell is CellState.ALIVE or cell is CellState.BORN:
                    if alive_around < 2 or alive_around > 3:
                        self.world.set_cell(x, y, CellState.SICK)
                    else:
                        self.world.set_cell(x, y, CellState.ALIVE)
                else:
                    if alive_around == 3:
                        self.world.set_cell(x, y, CellState.BORN)
                    else:
                        self.world.set_cell(x, y, CellState.DEAD)


def print_world(world: World, step_no: int):
    hl: str = ' ' + '-' * (world.x_size * 2 - 1)
    header: str = f'Step: {step_no}'
    print(f'{header} {hl[len(header) + 1:]}')

    for row in world.world_map:
        print(f'|{" ".join(list(map(str, row)))}|')

    print(hl)


def main() -> NoReturn:
    # Есть проблема из-за того, что используется только одно зацикленное поле:
    # Когда проверяется ячейка обработанная в прошлом (находящаяся слева и сверху), то можно получить значение ячейки из
    # будущего (находящееся в нижней строке или правом столбце).
    # И наоборот, проверяя ячейку из будущего, можно получить значение ячейки из прошлого.

    world = World([
        '         xx                                              ',
        '           x                                             ',
        '            x                                            ',
        '             x                                           ',
        '            xx            x                              ',
        '                           xx      x                     ',
        '         xx                  x    x                      ',
        '                                 x                       ',
        '                                                         ',
        '                              x                          ',
        '      x   x x x x xxxx                                   ',
        '           xxxxxxxx                                      ',
        '                xxx                                      ',
        '                                                         ',
        '                   x                                     ',
        '                    x                                    ',
        '                     x                                   ',
        '                                            x            ',
        '                                    x  x   x             ',
        '                                     x xx x              ',
        '                                        xx               ',
        '                                      x xxx              ',
        '                                       x  x              ',
        '                                                         ',
        '                                            xx           ',
        '     x                                     xxxxx         ',
        '     xx                                    xx            ',
        '   xxx                                    x  x           ',
        '                                                         ',
        '                                                         ',
    ])
    life = Life(world)
    for step in range(1, 1_000_000):
        life.step()
        print_world(world=world, step_no=life.step_no)
        time.sleep(.1)


if __name__ == '__main__':
    main()
