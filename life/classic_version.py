import sys
import time
import enum

'''Rules:
- Any live cell with fewer than two live neighbours dies.
- Any live cell with more than three live neighbours dies.
- Any dead cell with exactly three live neighbours becomes a live cell.
'''


class CellState(enum.StrEnum):
    DEAD = ' '
    ALIVE = 'O'


class World:
    _world_map: list[list[CellState]]
    x_size: int
    y_size: int

    def __init__(self, x_size: int, y_size: int):
        self.x_size = x_size
        self.y_size = y_size
        self._world_map = [[CellState.DEAD for _ in range(self.x_size)] for _ in range(self.y_size)]

    def set_cell(self, x: int, y: int, state: CellState):
        self._world_map[y % self.y_size][x % self.x_size] = state

    def get_cell(self, x: int, y: int) -> CellState:
        return self._world_map[y % self.y_size][x % self.x_size]


def print_world(world: World, step_no: int):
    hl: str = ' ' + '-' * (world.x_size * 2 - 1)
    header: str = f'Step: {step_no}'
    print(f'{header} {hl[len(header) + 1:]}')

    for y in range(world.y_size):
        row: list[CellState] = [world.get_cell(x, y) for x in range(world.x_size)]
        print(f'|{" ".join(list(map(str, row)))}|')

    print(hl)


def life_step(world: World) -> World:
    new_world: World = World(world.x_size, world.y_size)
    for y in range(world.y_size):
        for x in range(world.x_size):
            cell_alive: bool = world.get_cell(x, y) == CellState.ALIVE
            live_neighbours: int = sum(
                [
                    world.get_cell(x + dx, y + dy) == CellState.ALIVE
                    for dx, dy in ((-1, -1), (0, -1), (1, -1), (-1, 0), (1, 0), (-1, 1), (0, 1), (1, 1))
                ],
                0
            )
            if cell_alive:
                if live_neighbours < 2 or live_neighbours > 3:
                    new_world.set_cell(x, y, CellState.DEAD)
                else:
                    new_world.set_cell(x, y, CellState.ALIVE)
            else:
                if live_neighbours == 3:
                    new_world.set_cell(x, y, CellState.ALIVE)

    return new_world


def main():
    init_data: list[list[bool]] = []
    with open('init_world.txt', 'r') as f:
        for line in f:
            init_data.append([a == 'O' for a in line.rstrip()])
    x_size: int = max(*map(len, init_data))
    y_size: int = len(init_data)
    world: World = World(x_size, y_size)
    for y, row in enumerate(init_data):
        for x, alive in enumerate(row):
            if alive:
                world.set_cell(x, y, CellState.ALIVE)

    print_world(world, 0)
    for step in range(1, 1000):
        time.sleep(.2)
        world: World = life_step(world)
        print_world(world, step)


if __name__ == '__main__':
    main()
    sys.exit(0)
