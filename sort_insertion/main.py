# encoding: UTF-8
import random
from copy import copy

__doc__ = """Insertion sorting algorithm."""


def insertion(sequence: list[int]) -> list[int]:
    # Элементы входной последовательности просматриваются по одному, и каждый новый поступивший элемент размещается в
    # подходящее место среди ранее упорядоченных элементов.
    # Для ускорения можно искать место для элемента не последовательно перебирая все числа слева, а делением пополам.
    # Тогда при сдвиге чисел между старым и новым местом элемента можно не сравнивать их значения.

    result: list[int] = []
    for number in sequence:
        index_from: int = 0
        index_to: int = len(result)
        while index_to - index_from > 0:
            index = index_from + int((index_to - index_from) / 2)
            if result[index] < number:
                index_from = index + 1
            elif number < result[index]:
                index_to = index
            elif result[index] == number:
                break
        else:
            index: int = index_from

        result.insert(index, number)

    return result


if __name__ == '__main__':
    random_sequence: list[int] = [random.randint(10, 99) for _ in range(40)]
    valid_result: list = sorted(random_sequence)

    result = insertion(copy(random_sequence))
    for num in result:
        print('{0:>3}: {1:->{0}}'.format(num, '|'))
    assert valid_result == insertion(copy(random_sequence))
