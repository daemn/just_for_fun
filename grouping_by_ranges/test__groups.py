# encoding: UTF-8
import random
from copy import copy
from typing import NamedTuple
import pytest

from grouping_by_ranges.groups import GroupedSequence


class TestGroups:
    random.seed(42)

    class Params(NamedTuple):
        id: str
        sequence: list
        groups_limit: int

    @pytest.fixture(params=(
        Params(id='random float', sequence=[random.random() for _ in range(100)], groups_limit=10),
        Params(id='random int', sequence=[random.randint(0, 1_000_000_000) for _ in range(100)], groups_limit=10),
        Params(id='ordered int', sequence=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], groups_limit=3),
        Params(id='reversed ordered int', sequence=[15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1], groups_limit=3),
    ), ids=lambda a: a.id)
    def params_item(self, request) -> Params:
        return request.param

    def test__groups(self, params_item: Params):
        grouped: GroupedSequence = GroupedSequence(copy(params_item.sequence), limit=params_item.groups_limit)

        # List items didn't change. Items order changed.
        assert sorted(params_item.sequence) == sorted(grouped.sequence)

        # Number of groups limited by groups_limit parameter.
        assert len(grouped.groups) <= params_item.groups_limit

        # All groups have items.
        assert all((a.quantity > 0 for a in grouped.groups))

        # Sum of all items in groups is equal to length of sequence.
        assert sum((a.quantity for a in grouped.groups)) == len(params_item.sequence)

        # Value_from less or equal value_to.
        assert all((a.value_from <= a.value_to for a in grouped.groups))

        # Index_from less index_to.
        assert all((a.index_from < a.index_to for a in grouped.groups))

        # Group items number is equal to group index range.
        assert all((a.index_to - a.index_from == a.quantity for a in grouped.groups))

        for i in range(len(grouped.groups) - 1):
            # Groups in sorted order.
            assert grouped.groups[i].value_to < grouped.groups[i + 1].value_from
            # Group end is the next group begin.
            assert grouped.groups[i].index_to == grouped.groups[i + 1].index_from

        for group in grouped.groups:
            # All items in group are in group range.
            assert all((
                group.value_from <= a <= group.value_to
                for a in grouped.sequence[group.index_from:group.index_to]
            ))
