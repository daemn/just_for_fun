# encoding: UTF-8
from typing import Generator, Any
from dataclasses import dataclass, field

__doc__ = """The RangeList class collects rough statistic about the distribution of values.

>>> ranges = RangeList(limit=3)
>>> ranges.add_value(1)
>>> ranges.add_value(4)
>>> ranges.add_value(4)
>>> ranges.add_value(2)
>>> ranges.add_value(6)
>>> ranges.add_value(5)
>>> ranges.add_value(5)
>>> list(ranges)
[
    Range(value_from=1, value_to=2, quantity=2),
    Range(value_from=4, value_to=5, quantity=3),
    Range(value_from=6, value_to=6, quantity=2)
]
"""


@dataclass
class Range:
    value_from: Any
    value_to: Any
    quantity: int
    prev: 'Range' = field(default=None, repr=False)
    next: 'Range' = field(default=None, repr=False)

    @classmethod
    def new(cls, value: Any) -> 'Range':
        return Range(value_from=value, value_to=value, quantity=1)


class RangeList:
    _limit: int
    _head: Range = None
    _length: int = 0

    def __init__(self, limit: int):
        """
        :param limit: The limit of ranges number in this list.
        """
        self._limit = limit

    def __len__(self) -> int:
        return self._length

    def __iter__(self) -> Generator:
        cur: Range = self._head
        while cur is not None:
            yield cur
            cur = cur.next

    def _insert_new_before(self, value: Any, range_: Range):
        new: Range = Range.new(value)
        prev: Range = range_.prev
        if prev is None:
            self._head = new
        else:
            prev.next = new
            new.prev = prev
        new.next = range_
        range_.prev = new
        self._length += 1

    def _insert_new_after(self, value: Any, range_: Range):
        new: Range = Range.new(value)
        if range_.next is not None:
            range_.next.prev = new
            new.next = range_.next
        range_.next = new
        new.prev = range_
        self._length += 1

    def _shrink_list(self):
        range_: Range = min(
            filter(lambda a: a.next is not None, self),
            key=lambda a: a.quantity + a.next.quantity
        )
        range_.quantity += range_.next.quantity
        range_.value_to = range_.next.value_to
        range_.next = range_.next.next
        if range_.next is not None:
            range_.next.prev = range_
        self._length -= 1

    def add_value(self, value: Any):
        """
        :param value: New value for counting.
        """
        if self._head is None:
            self._head = Range.new(value)
            self._length = 1
            return

        cursor: Range = self._head
        while cursor.next is not None and value > cursor.value_to:
            cursor = cursor.next

        if value < cursor.value_from:
            self._insert_new_before(value=value, range_=cursor)
        elif value <= cursor.value_to:
            cursor.quantity += 1
        else:
            self._insert_new_after(value=value, range_=cursor)

        if self._length > self._limit:
            self._shrink_list()
