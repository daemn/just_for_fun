# encoding: UTF-8
import random
import string
from typing import NamedTuple
import pytest

from grouping_by_ranges.ranges import RangeList, Range


class TestRandomSequence:
    class Params(NamedTuple):
        id: str
        sequence: list
        ranges_limit: int
        range_answer: list[tuple] = None

    @pytest.fixture(params=(
        Params(
            id='chars',
            sequence=[random.choice(string.ascii_letters) for _ in range(1_000)],
            ranges_limit=10,
        ),
        Params(
            id='float values',
            sequence=[random.random() for _ in range(1_000)],
            ranges_limit=10,
        ),
        Params(
            id='value range less than range limit',
            sequence=[random.randint(0, 5) for _ in range(1_000)],
            ranges_limit=10,
        ),
        Params(
            id='small value range',
            sequence=[random.randint(0, 10) for _ in range(1_000)],
            ranges_limit=5,
        ),
        Params(
            id='short seq, big range',
            sequence=[random.randint(0, 1_000_000_000) for _ in range(1_000)],
            ranges_limit=10,
        ),
        Params(
            id='average case',
            sequence=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            ranges_limit=3,
            range_answer=[(0, 3, 4), (4, 6, 3), (7, 9, 3)],
        ),
        Params(
            id='bad case',
            sequence=[0, 0, 0, 1, 1, 1, 2, 9, 3, 4, 5, 6, 7, 8],
            ranges_limit=3,
            range_answer=[(0, 0, 3), (1, 1, 3), (2, 9, 8)],
        ),
    ), ids=lambda a: a.id)
    def params_item(self, request) -> Params:
        return request.param

    def test__ranges(self, params_item: Params):
        random.seed(42)

        ranges: RangeList = RangeList(limit=params_item.ranges_limit)
        for value in params_item.sequence:
            ranges.add_value(value)
        ranges: list[Range] = list(ranges)

        # Number of ranges limited by ranges_limit parameter.
        assert len(ranges) <= params_item.ranges_limit

        # All ranges have items.
        assert all((a.quantity > 0 for a in ranges))

        # Sum of all items in ranges is equal to length of sequence.
        assert sum((a.quantity for a in ranges)) == len(params_item.sequence)

        # Value_from less or equal value_to.
        assert all((a.value_from <= a.value_to for a in ranges))

        range_: Range
        for i in range(len(ranges) - 1):
            # Groups in sorted order.
            assert ranges[i].value_to < ranges[i + 1].value_from

        # All items source sequence belongs one and only one range.
        sequence: list = params_item.sequence
        for range_ in ranges:
            one_range_items: list = list(filter(lambda a: range_.value_from <= a <= range_.value_to, sequence))
            sequence = list(filter(lambda a: not (range_.value_from <= a <= range_.value_to), sequence))
            assert len(one_range_items) == range_.quantity
        assert len(sequence) == 0

        # The ranges are exactly like this.
        if params_item.range_answer is not None:
            assert [(a.value_from, a.value_to, a.quantity) for a in ranges] == params_item.range_answer
