# encoding: UTF-8
from typing import Any
from dataclasses import dataclass, field

from grouping_by_ranges.ranges import RangeList, Range

__doc__ = """The algorithm groups values by ranges.

>>> grouped = GroupedSequence([9, 0, 5, 8, 7, 6, 3, 3, 2], limit=3)
>>> grouped.groups
[
    Group(value_from=0, value_to=6, index_from=0, index_to=6, quantity=6),
    Group(value_from=7, value_to=8, index_from=6, index_to=8, quantity=2),
    Group(value_from=9, value_to=9, index_from=8, index_to=9, quantity=1)
]
>>> grouped.sequence
[2, 0, 5, 3, 3, 6, 8, 7, 9]
"""


@dataclass
class Group:
    value_from: Any
    value_to: Any
    index_from: int
    index_to: int
    quantity: int
    cursor: int = field(repr=False)

    def is_value_should_be_here(self, value: Any) -> bool:
        return self.value_from <= value <= self.value_to


class GroupedSequence:
    sequence: list[Any]
    groups: list[Group]
    _limit: int

    def __init__(self, sequence: list[Any], limit: int):
        """Groups values by ranges.

        :param sequence: Source sequence with random values.
        :param limit: More groups mean more accuracy but more memory usage.
        """
        self.sequence = sequence
        self._limit = limit
        self._detect_groups()
        self._group_values_in_sequence()

    def _detect_groups(self):
        ranges: RangeList = RangeList(limit=self._limit)
        for value in self.sequence:
            ranges.add_value(value)

        index: int = 0
        self.groups = []
        range_: Range
        for range_ in ranges:
            self.groups.append(Group(
                value_from=range_.value_from,
                value_to=range_.value_to,
                quantity=range_.quantity,
                index_from=index,
                index_to=index + range_.quantity,
                cursor=index
            ))
            index += range_.quantity

    def _group_values_in_sequence(self):
        group: Group
        for group in self.groups:
            while group.cursor < group.index_to:
                value: Any = self.sequence[group.cursor]
                if group.is_value_should_be_here(value):
                    group.cursor += 1
                    continue

                target_group: Group = self._find_group_by_value(value)
                while target_group != group:
                    suspicious_value: Any = self.sequence[target_group.cursor]
                    while target_group.is_value_should_be_here(suspicious_value):
                        target_group.cursor += 1
                        suspicious_value = self.sequence[target_group.cursor]
                    value, self.sequence[target_group.cursor] = suspicious_value, value
                    target_group.cursor += 1
                    target_group = self._find_group_by_value(value)

                self.sequence[group.cursor] = value
                group.cursor += 1

    def _find_group_by_value(self, value: Any, index_from: int = None, index_to: int = None) -> Group:
        if index_from is None:
            index_from = 0
        if index_to is None:
            index_to = len(self.groups)
        index: int = index_from + (index_to - index_from) // 2
        if value < self.groups[index].value_from:
            return self._find_group_by_value(value, index_from=index_from, index_to=index)
        elif self.groups[index].value_to < value:
            return self._find_group_by_value(value, index_from=index + 1, index_to=index_to)
        return self.groups[index]
