import random
from copy import copy

from grouping_by_ranges.ranges import RangeList, Range
from grouping_by_ranges.groups import GroupedSequence


def get_by_index(sequence: list[int], index: int) -> int:
    # Detect ranges.
    ranges: RangeList = RangeList(limit=len(sequence) // 1000)
    for item in sequence:
        ranges.add_value(item)

    # Search range that contains index.
    range_: Range
    for range_ in ranges:
        if range_.quantity <= index:
            index -= range_.quantity
        else:
            print(f'Found range: {range_}.')
            break

    # Get item from ranged subsequence.
    subsequence: list[int] = list(filter(lambda a: range_.value_from <= a <= range_.value_to, sequence))
    subsequence.sort()
    return subsequence[index]


def get_slice(sequence: list[int], index_from: int, index_to: int) -> list[int]:
    # Detect ranges.
    ranges: RangeList = RangeList(limit=len(sequence) // 1000)
    for item in sequence:
        ranges.add_value(item)

    subranges: list[Range] = []
    slice_size: int = index_to - index_from

    # Search ranges that contain items.
    range_: Range
    for range_ in ranges:
        if range_.quantity <= index_from:
            index_from -= range_.quantity
            index_to -= range_.quantity
            continue
        print(f'Found range: {range_}.')
        subranges += [range_]
        index_to -= range_.quantity
        if index_to < 0:
            break

    # Get items that we need.
    subsequence: list[int] = list(filter(lambda a: subranges[0].value_from <= a <= subranges[-1].value_to, sequence))
    subsequence.sort()
    return subsequence[index_from:index_from + slice_size]


if __name__ == '__main__':
    random.seed(42)
    sequence: list[int] = [random.randint(0, 1_000_000) for _ in range(100_000)]

    print('\n# Get an item by index.\n')
    item: int = get_by_index(copy(sequence), index=3000)
    print(f'Item 3000 is {item}.')

    print('\n# Get the median.\n')
    item: int = get_by_index(copy(sequence), index=len(sequence) * 50 // 100)
    print(f'Median is {item}.')

    print('\n# Get a percentile.\n')
    item: int = get_by_index(copy(sequence), index=len(sequence) * 90 // 100)
    print(f'Percentile 90 is {item}.')

    print('\n# Get a slice.\n')
    items: list[int] = get_slice(copy(sequence), index_from=700, index_to=750)
    print(f'Items are {items}.')

    sequence: list[int] = [random.randint(0, 9) for _ in range(100)]
    print('\n# Group a random sequence.\n')
    grouped_sequence: GroupedSequence = GroupedSequence(sequence, limit=5)
    for group in grouped_sequence.groups:
        print(f'{group}: {grouped_sequence.sequence[group.index_from:group.index_to]}')
