# encoding: UTF-8
import random
from copy import copy

__doc__ = """Comb sorting algorithm."""


def comb(sequence: list[int]) -> list[int]:
    # Основная идея «расчёски» в том, чтобы первоначально брать достаточно большое расстояние между сравниваемыми
    # элементами и по мере упорядочивания массива сужать это расстояние вплоть до минимального. Таким образом, мы как бы
    # причёсываем массив, постепенно разглаживая на всё более аккуратные пряди. Первоначальный разрыв между
    # сравниваемыми элементами лучше брать с учётом специальной величины, называемой фактором уменьшения, оптимальное
    # значение которой равно примерно 1,247. Сначала расстояние между элементами максимально, то есть равно размеру
    # массива минус один. Затем, пройдя массив с этим шагом, необходимо поделить шаг на фактор уменьшения и пройти по
    # списку вновь. Так продолжается до тех пор, пока разность индексов не достигнет единицы. В этом случае сравниваются
    # соседние элементы, как и в сортировке пузырьком, но такая итерация одна.

    sequence_size: int = len(sequence)
    delta: int = sequence_size - 1
    while delta >= 1:
        for i in range(sequence_size - delta):
            if sequence[i] > sequence[i + delta]:
                sequence[i], sequence[i + delta] = sequence[i + delta], sequence[i]
        delta = int(delta // 1.247)

    return sequence


if __name__ == '__main__':
    random_sequence: list[int] = [random.randint(10, 99) for _ in range(100)]
    valid_result: list = sorted(random_sequence)
    assert valid_result == comb(copy(random_sequence))
