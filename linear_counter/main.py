# encoding: UTF-8
from random import randint
import pickle
from hashlib import blake2b
from typing import Any, Iterator
from pytest import fixture, raises

__doc__ = """How many different items are in a stream data. Explaining: https://youtu.be/eVDWMRPsjo4"""


class BitArray:
    """DIY bit array.

    ba = BitArray(20)
    ba[5] = 1
    ba[5]       #> 1
    ba[14]      #> 0
    """
    __slots__ = ('_length', '_array')
    _length: int
    _array: bytearray

    def __init__(self, array_length: int):
        """
        :param array_length: The number of stored bits.
        """
        self._length = array_length
        bytes_number: int = array_length // 8 + bool(array_length % 8)
        self._array = bytearray([0 for _ in range(bytes_number)])

    def __getitem__(self, index) -> int:
        if index < 0 or self._length <= index:
            raise IndexError
        byte_i, bit_i = divmod(index, 8)
        bit_mask: int = pow(2, 7 - bit_i)
        return 1 if (self._array[byte_i] & bit_mask) else 0

    def __setitem__(self, index, value):
        if index < 0 or self._length <= index:
            raise IndexError
        byte_i, bit_i = divmod(index, 8)
        bit_mask: int = pow(2, 7 - bit_i)
        if value:
            self._array[byte_i] |= bit_mask
        else:
            self._array[byte_i] &= (bit_mask ^ 255)

    def __repr__(self) -> str:
        bit_string: str = ''.join(['{:0>8b}'.format(a) for a in self._array])
        bit_string = bit_string[:self._length]
        return f'<{self.__class__.__name__}, {bit_string}>'

    def __len__(self) -> int:
        return self._length

    def __iter__(self) -> Iterator[int]:
        for i in range(self._length):
            yield self[i]


class LinearCounter:
    """Count unique items in a stream.

    counter = LinearCounter(5000)
    counter.count_item('item')
    print(counter.number_of_uniq_items)
    """
    __slots__ = ('array', 'hash_size', '_coefficient')
    array: BitArray
    hash_size: int      # Size of generated item hash.
    _coefficient: int   # Coefficient for translate hash to array index.

    def __init__(self, possible_uniq_items: int):
        """
        :param possible_uniq_items: A number of uniq items.
        The more possible unique items -- the greater accuracy of the count.
        """
        self.hash_size = 1
        while pow(256, self.hash_size) < possible_uniq_items:
            self.hash_size += 1

        self._coefficient = pow(256, self.hash_size) // possible_uniq_items

        max_hash_value = pow(256, self.hash_size)
        array_size = max_hash_value // self._coefficient
        self.array = BitArray(array_size)

    def _get_item_index(self, item: Any) -> int:
        hash_str: bytes = blake2b(pickle.dumps(item), digest_size=self.hash_size).digest()
        return int(hash_str.hex(), 16) // self._coefficient

    def remember_item(self, item: Any):
        """Count item if it unique."""
        self.array[self._get_item_index(item)] = 1

    def is_item_uniq(self, item: Any) -> int:
        """Return True when the item unique or False when may be not."""
        return self.array[self._get_item_index(item)] == 0

    def number_of_uniq_items(self) -> int:
        """Return number of unique items."""
        return sum(self.array, 0)

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__}, hash_size: {self.hash_size}, array_size: {len(self.array)}>'


class TestBitArray:

    @fixture
    def ba(self) -> BitArray:
        return BitArray(10)

    def test__length(self, ba: BitArray):
        assert len(ba) == 10

    def test__index_range(self):
        ba: BitArray = BitArray(3)
        assert ba[2] == 0
        assert ba[1] == 0

        ba: BitArray = BitArray(5)
        assert ba[0] == 0
        assert ba[3] == 0
        with raises(IndexError):
            _ = ba[-1]
        with raises(IndexError):
            _ = ba[-3]
        with raises(IndexError):
            _ = ba[5]
        with raises(IndexError):
            _ = ba[9]

    def test__set_get(self, ba: BitArray):
        ba[5] = 1
        ba[3] = 1
        ba[7] = 1
        ba[5] = 0

        assert ba[1] == 0
        assert ba[3] == 1
        assert ba[5] == 0
        assert ba[7] == 1

    def test__iteration(self, ba: BitArray):
        ba[2] = 1
        ba[8] = 1
        for index, bit in enumerate(ba):
            if index in (2, 8):
                assert bit == 1
            else:
                assert bit == 0


class TestLinearCounter:

    @fixture
    def lc(self) -> LinearCounter:
        return LinearCounter(100)

    def test__remember_item(self, lc: LinearCounter):
        lc.remember_item(3)
        lc.remember_item({'a': 1, 'b': 2})
        lc.remember_item(True)
        lc.remember_item(['foo', 'bar'])
        lc.remember_item(b'foo bar')

    def test__is_item_uniq(self, lc: LinearCounter):
        assert lc.is_item_uniq(5) is True

        lc.remember_item(3)
        assert lc.is_item_uniq(3) is False

        lc.remember_item('Papa Carlo')
        assert lc.is_item_uniq('Papa Carlo') is False

    def test__number_of_uniq_items(self, lc: LinearCounter):
        lc.remember_item(3)
        lc.remember_item(5)
        lc.remember_item('Papa Carlo')
        lc.remember_item({'a': 'foo', 'b': 'bar'})

        number: int = lc.number_of_uniq_items()
        assert 0 < number < 5

    def test__many_items(self, lc: LinearCounter):
        counter: LinearCounter = LinearCounter(5000)
        for _ in range(1000):
            counter.remember_item(randint(1, 500))
        counter.remember_item(100)
        counter.remember_item(100)
        assert counter.is_item_uniq(100) is False
        assert counter.is_item_uniq(501) is True
