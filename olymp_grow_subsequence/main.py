#!/usr/bin/env python3
# encoding: UTF-8

# There is a list of integer numbers X1, X2, ..., Xn. We need to remove the minimum quantity of numbers from the list in
# order to sort the list in ascending order.
#
# 1 <= n <= 100
# 0 <= Xi <= 99

import functools
from copy import copy
from dataclasses import dataclass


def method_5(src_numbers: list[int]) -> list[int]:
    # TODO: think about it.
    #
    #     7                 7
    #     :           6     : 6
    #     :         5 :     : 5
    #     : 4       : :     4 :
    #   3 : :     3 : :  => 3 3
    # 2 : : :   2 : : :     2 2
    # : : : : 1 : : : :     : 1
    # \___/_V \_______/
    #   |   |     (5) БОльшая посл.
    #   |   Сопутствующий ущерб
    #   (3+1) Меньшая посл.
    pass


def off_method_1(src_numbers: list[int]):
    # 1st method. Enumeration of all options. Using a mask.

    def mask_iter(quantity=1):
        mask = [1] * quantity
        while True:
            yield mask
            i = 0
            while i < quantity:
                if mask[i] == 1:
                    mask[i] = 0
                    break
                mask[i] = 1
                i += 1
            else:
                return None

    max_stored = 0
    min_mask = []
    quantity = len(src_numbers)
    for mask in mask_iter(quantity):
        if functools.reduce(lambda x, y: x + y, mask) < max_stored:
            continue
        prev_val = 0
        for i in range(quantity):
            if mask[i] == 0: continue
            if src_numbers[i] > prev_val:
                prev_val = src_numbers[i]
                continue
            break
        else:
            min_mask = list(mask)
            max_stored = functools.reduce(lambda x, y: x + y, mask)

    answer = []
    for i in range(quantity):
        if min_mask[i]:
            answer.append(src_numbers[i])

    return answer


def method_2(numbers: list[int]):
    # 2nd method. Remember all growing up sequences from every number in list.

    results = []
    for number in numbers:
        add_results = []
        for result in results:
            if number > result[-1]:
                add_results.append(list(result))
                add_results[-1].append(number)
        if len(add_results) > 0:
            results += add_results
        results.append([number])

    max_stored = 0
    answer = []
    for i in results:
        if len(i) > max_stored:
            max_stored = len(i)
            answer = list(i)

    return answer


def method_3(numbers):
    # 3rd method. Remember all growing up sequences from every number in list.

    results = dict()
    for number in numbers:
        add_results = dict()
        for result, r_num in results.items():
            if number > r_num:
                add_results[result + ':' + str(number)] = number
        if len(add_results) > 0:
            results.update(add_results)
        results[str(number)] = number

    max_stored = 0
    answer = []
    for k, v in results.items():
        result = k.split(':')
        if len(result) > max_stored:
            max_stored = len(result)
            answer = list(map(int, result))

    return answer


def method_4(source_numbers: list[int]):
    # 4th method. Enumeration of options again.
    # Copies of sequences and parameters are stored in the stack. The program will return to these stored points later
    # and try to find another solution.

    @dataclass
    class Fall:
        potential: bool = None
        start: int = None
        quantity: int = None
        sol_num: int = None
        index: int = None
        nums: list[int] = None

    def find_solutions(index: int, num: list[int], sol_num: int) -> Fall:
        quantity: int = 1
        nums_len: int = len(num)
        start: int = index

        while True:
            if start < 1 or start + quantity >= nums_len:
                break
            if num[start - 1] < num[start + quantity]:
                sol_num -= 1
                if sol_num == 0:
                    break
            start -= 1
            if start + quantity < index:
                quantity += 1
                start = index
        return Fall(
            potential=sol_num <= 1,
            start=start,
            quantity=quantity
        )

    answer = []
    answer_len = 0
    stack = []
    index = 1
    sol_num = 1
    nums = list(source_numbers)

    while index < len(nums) or len(stack) > 0:
        if index < len(nums):
            if nums[index - 1] < nums[index]:
                index += 1
            else:
                solution: Fall = find_solutions(index, nums, sol_num)
                stack.append(Fall(
                    index=index,
                    sol_num=sol_num,
                    potential=solution.potential,
                    nums=list(nums)
                ))
                for _ in range(solution.quantity):
                    nums.pop(solution.start)
                if index > solution.start:
                    index = solution.start
                    if index == 0:
                        index = 1
        else:
            if len(nums) > answer_len:
                answer_len = len(nums)
                answer = list(nums)
            while len(stack) > 0:
                fall = stack.pop()
                if fall.potential:
                    sol_num = fall.sol_num + 1
                    index = fall.index
                    nums = list(fall.nums)
                    break

    return answer


def remove_peaks_and_valleys(source_numbers: list[int]) -> list[int]:
    result_numbers: list[int] = copy(source_numbers)
    list_is_sorted: bool = False
    while not list_is_sorted:
        list_is_sorted = True

        highest_peak_index: int = -1
        list_length: int = len(result_numbers)
        for i in range(list_length - 1):
            if (
                (i == 0 or result_numbers[i - 1] < result_numbers[i])
                and result_numbers[i] > result_numbers[i + 1]
            ):
                list_is_sorted = False
                if highest_peak_index == -1 or result_numbers[i] > result_numbers[highest_peak_index]:
                    highest_peak_index = i

        if highest_peak_index > -1:
            result_numbers.pop(highest_peak_index)

        lowest_valley_index: int = -1
        list_length: int = len(result_numbers)
        for i in range(1, list_length):
            if (
                result_numbers[i - 1] > result_numbers[i]
                and (i == (list_length - 1) or result_numbers[i] < result_numbers[i + 1])
            ):
                list_is_sorted = False
                if lowest_valley_index == -1 or result_numbers[i] <= result_numbers[lowest_valley_index]:
                    lowest_valley_index = i

        if lowest_valley_index > -1:
            result_numbers.pop(lowest_valley_index)

    return result_numbers


def draw_graph(number_list: list):
    scaled_list: list[int] = list(map(lambda a: a // 10, number_list))
    for row_num in range(10, -1, -1):
        compared_row: list[int] = [(a > row_num) - (a < row_num) for a in scaled_list]
        print('  '.join([(' ', '_', '|')[a + 1] for a in compared_row]))

    print(''.join(['{:<3}'.format(a) for a in number_list]))


if __name__ == '__main__':
    numbers = [99, 88, 17, 33, 17, 75, 19, 67, 17, 95, 66, 19, 62,  4, 24, 95, 59, 73, 92, 47, 92, 14]
    draw_graph(numbers)

    for method in remove_peaks_and_valleys, method_2, method_3, method_4, off_method_1:
        print(f'\n{"-" * 30} Test {method.__name__}')
        result: list[int] = method(copy(numbers))
        draw_graph(result)
        print(f'Removed: {len(numbers) - len(result)}')
