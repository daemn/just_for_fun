# encoding: UTF-8

__doc__ = """Calculate the sum of arguments.
Example:
>>> get_sum(2)(6)(-1)()
7
"""


def chain_sum_list(arg=None, _sum=[]):
    if arg is None:
        result = sum(_sum)
        del _sum[:]
        return result
    _sum.append(arg)
    return chain_sum_list


class ChainSumClass:

    def __init__(self):
        self._sum = 0

    def __call__(self, arg=None):
        if arg is None:
            result = self._sum
            self._sum = 0
            return result
        self._sum += arg
        return self.__call__


def chain_sum_perfect(arg):
    def mem_chain_sum(arg2=None):
        if arg2 is None:
            return arg
        return chain_sum_perfect(arg + arg2)
    return mem_chain_sum


def chain_sum_perfect_except(arg):
    def mem_chain_sum(*args):
        try:
            return chain_sum_perfect_except(arg + args[0])
        except IndexError:
            return arg
    return mem_chain_sum


def chain_sum_super_perfect(arg):
    def mem_chain_sum(arg2=None):
        return {None: arg}.get(arg2, chain_sum_super_perfect(arg + (arg2 or 0)))
    return mem_chain_sum


def chain_sum_lambda(arg):
    return lambda arg2=None: chain_sum_lambda(arg + arg2) if arg2 is not None else arg


class ChainSumInt(int):
    def __call__(self, arg=0):
        return ChainSumInt(self + arg)


def main():
    for func in (
            chain_sum_list,
            ChainSumClass(),
            chain_sum_perfect,
            chain_sum_perfect_except,
            chain_sum_super_perfect,
            chain_sum_lambda,
            ChainSumInt(),
    ):
        assert func(5)() == 5
        assert func(5)(2)() == 7
        assert func(5)(0)(100)(-10)() == 95


if __name__ == '__main__':
    main()
