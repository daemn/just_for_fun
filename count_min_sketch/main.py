# encoding: UTF-8
import pickle
from hashlib import blake2b, sha512, md5
from typing import Any, List

__doc__ = """How many times item are in a stream data. Explaining: https://youtu.be/eVDWMRPsjo4"""


class MultiHash:
    """Compute few item hashes at the same time."""

    __slots__ = ('hash_set',)
    hash_set: List[bytes]

    def __init__(self, item: Any, hash_size: int):
        """
        :param item: Any item.
        :param hash_size: Length of computes hash in bytes.
        """
        serialize_item: bytes = pickle.dumps(item)
        self.hash_set = []
        self.hash_set.append(blake2b(serialize_item, digest_size=hash_size).digest())
        self.hash_set.append(sha512(serialize_item).digest()[:hash_size])
        self.hash_set.append(md5(serialize_item).digest()[:hash_size])


class CountMinSketch:
    """Count an items number."""

    __slots__ = ('hash_size', 'matrix', 'coefficient', 'hash_number')
    hash_size: int
    matrix: List[List[int]]
    coefficient: int
    hash_number: int

    def __init__(self, possible_uniq_items: int):
        """
        :param possible_uniq_items: A number of uniq items.
        The more possible unique items -- the greater accuracy of the count.
        """
        self.hash_size = 1
        while pow(256, self.hash_size) < possible_uniq_items:
            self.hash_size += 1

        test_hash: MultiHash = MultiHash('foo', self.hash_size)
        self.hash_number = len(test_hash.hash_set)

        higher_hash: int = pow(256, self.hash_size)
        self.coefficient = higher_hash // possible_uniq_items
        array_size: int = higher_hash // self.coefficient

        self.matrix = []
        for array in range(self.hash_number):
            self.matrix.append([0 for _ in range(array_size)])

    def _get_index_from_hash(self, hash_bytes: bytes) -> int:
        hash_dec: int = int(hash_bytes.hex(), 16)
        return hash_dec // self.coefficient

    def remember_item(self, item: Any):
        """Increase item counter."""
        multi_hash: MultiHash = MultiHash(item, self.hash_size)
        for array_number in range(len(self.matrix)):
            array_index: int = self._get_index_from_hash(multi_hash.hash_set[array_number])
            self.matrix[array_number][array_index] += 1

    def quantity_of_item(self, item: Any) -> int:
        """Return probably quantity of the item."""
        multi_hash: MultiHash = MultiHash(item, self.hash_size)
        quantity_list: List[int] = []
        for array_number in range(len(self.matrix)):
            array_index: int = self._get_index_from_hash(multi_hash.hash_set[array_number])
            quantity_list.append(self.matrix[array_number][array_index])
        return min(quantity_list)

    def __repr__(self) -> str:
        return (
            f'<{self.__class__.__name__}'
            f', hash_size: {self.hash_size}'
            f', coefficient: {self.coefficient}'
            f', array_size: {len(self.matrix[0])}>'
        )


class TestMultiHash:

    def test__init(self):
        mh: MultiHash = MultiHash('foo', 3)
        assert len(mh.hash_set) > 1
        assert all(map(lambda a: len(a) == 3, mh.hash_set)) is True

        mh: MultiHash = MultiHash('bar', 5)
        assert len(mh.hash_set) > 1
        assert all(map(lambda a: len(a) == 5, mh.hash_set)) is True


class TestCountMinSketch:

    def test__init(self):
        cms: CountMinSketch = CountMinSketch(10)
        assert cms.hash_size > 0
        assert cms.hash_number > 1
        assert cms.coefficient > 1
        assert len(cms.matrix[0]) >= 10
        assert len(cms.matrix[0]) <= pow(256, cms.hash_size)

    def test__count_items(self):
        cms: CountMinSketch = CountMinSketch(10)

        cms.remember_item('1')
        assert cms.quantity_of_item('1') == 1

        cms.remember_item('2')
        assert cms.quantity_of_item('2') == 1

        cms.remember_item('3')
        assert cms.quantity_of_item('3') == 1

        assert cms.quantity_of_item('4') == 0

    def test__count_many_items(self):
        cms: CountMinSketch = CountMinSketch(100)
        for item in range(20):
            cms.remember_item(item)
        for item in range(10, 30):
            cms.remember_item(item)
        for item in range(100):
            cms.remember_item(item)
        assert cms.quantity_of_item(0) == 2
        assert cms.quantity_of_item(9) == 2
        assert cms.quantity_of_item(11) == 3
        assert cms.quantity_of_item(25) == 2
