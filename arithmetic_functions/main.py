# encoding: UTF-8
from typing import Callable, Union

__doc__ = """Calculate value by chain of arithmetic functions.
E.g.:
>>> seven(times(five()))
35
"""


def reg_oper(oper: str) -> Callable:
    def func(arg: str) -> str:
        return oper + arg
    return func


def reg_num(num: str) -> Callable:
    def func(arg: str = None) -> Union[str, int, float]:
        if arg is None:
            return num
        expr: str = num + arg
        return eval(expr)
    return func


times: Callable = reg_oper('*')
plus: Callable = reg_oper('+')
minus: Callable = reg_oper('-')
div: Callable = reg_oper('/')

zero: Callable = reg_num('0')
one: Callable = reg_num('1')
two: Callable = reg_num('2')
three: Callable = reg_num('3')
four: Callable = reg_num('4')
five: Callable = reg_num('5')
six: Callable = reg_num('6')
seven: Callable = reg_num('7')
eight: Callable = reg_num('8')
nine: Callable = reg_num('9')


if __name__ == '__main__':
    assert seven(times(five())) == 35
    assert four(plus(nine())) == 13
    assert zero(plus(two())) == 2
    assert eight(minus(three())) == 5
    assert six(div(two())) == 3.0
