#!/usr/bin/env python3
# encoding: UTF-8
from dataclasses import dataclass
import math

# There is a triangle and a point. Check that the point belongs to the triangle.


@dataclass
class Point:
    x: int
    y: int


@dataclass
class Triangle:
    a: Point
    b: Point
    c: Point


def check_angles(triangle: Triangle, check_point: Point) -> bool:
    """Checks angles between triangle segments and a segment from triangle vertex to check point."""

    def angle(point_from: Point, point_to: Point) -> float:
        """Returns the angle between two segments."""
        dx: int = point_from.x - point_to.x
        dy: int = point_from.y - point_to.y
        if dy == 0 and dx < 0:
            return math.pi
        r: float = math.sqrt(dx**2 + dy**2)
        a: float = math.acos(dx/r)
        if dy < 0:
            a = 2 * math.pi - a
        return a

    vertex: Point
    point_1: Point
    point_2: Point
    for vertex, point_1, point_2 in (
            (triangle.a, triangle.b, triangle.c),
            (triangle.b, triangle.c, triangle.a),
            (triangle.a, triangle.c, triangle.b)
    ):
        angle_1: float = angle(vertex, point_1)
        angle_2: float = angle(vertex, point_2)
        angle_c: float = angle(vertex, check_point)
        min_angle: float = min([angle_1, angle_2])
        max_angle: float = max([angle_1, angle_2])
        if (max_angle - min_angle) < math.pi:
            if angle_c < min_angle or angle_c > max_angle:
                return False
        else:
            if min_angle < angle_c < max_angle:
                return False

    return True


def check_sides(triangle: Triangle, check_point: Point) -> bool:
    """Checks that the point and the opposite vertex of the triangle are located on the same side of the triangle
     segment."""
    point_1: Point
    point_2: Point
    point_opposite: Point
    for point_1, point_2, point_opposite in (
            (triangle.a, triangle.b, triangle.c),
            (triangle.b, triangle.c, triangle.a),
            (triangle.a, triangle.c, triangle.b)
    ):
        if point_1.x == point_2.x:  # The segment is vertical.
            if check_point.x < point_1.x < point_opposite.x:
                return False
            if point_opposite.x < point_1.x < check_point.x:
                return False
        elif point_1.y == point_2.y:  # The segment is horizontal.
            if check_point.y < point_1.y < point_opposite.y:
                return False
            if point_opposite.y < point_1.y < check_point.y:
                return False
        else:
            k: float = (point_1.x - point_2.x) / (point_1.y - point_2.y)
            check_point_project_y: float = point_1.y + (check_point.x - point_1.x) / k
            opposite_point_project_y: float = point_1.y + (point_opposite.x - point_1.x) / k
            if check_point_project_y < check_point.y and opposite_point_project_y > point_opposite.y:
                return False
            if check_point_project_y > check_point.y and opposite_point_project_y < point_opposite.y:
                return False

    return True


if __name__ == '__main__':
    #    0123456789
    #  0 F
    #  1          B
    #  2         /|
    #  3        / |    Triangle: A (1,9); B (9,1); C (9,7)
    #  4       /  |    Not belongs: F (0,0); G (2,5); E (8.8)
    #  5   G  / H |    Belongs: H (7,5); D (9,6)
    #  6     /    D
    #  7    /   --C
    #  8   / --- E
    #  9  A--
    triangle = Triangle(Point(1, 9), Point(9, 1), Point(9, 7))
    for f in check_sides, check_angles:

        for point in Point(0, 0), Point(2, 5), Point(8, 8):
            assert f(triangle, point) is False

        for point in Point(7, 5), Point(9, 6):
            assert f(triangle, point) is True
