#!/usr/bin/env python3
# encoding: UTF-8
from typing import Iterator
from copy import copy
from dataclasses import dataclass

# There is a list of different symbols. Print all combinations of this list.


def combine_with_recurse(alph: str) -> Iterator[str]:
    for index in range(len(alph)):
        sub_alph: list[str] = list(alph)
        char: str = sub_alph.pop(index)
        if sub_alph:
            for other_chars in combine_with_recurse(''.join(sub_alph)):
                yield char + other_chars
        else:
            yield char


def combine_with_stack(alph: str) -> Iterator[str]:

    @dataclass
    class Item:
        alphabet: list[str]
        out_row: str

    stack: list[Item] = [Item(alphabet=list(alph), out_row='')]
    while stack:
        item: Item = stack.pop(-1)
        if item.alphabet:
            for index in range(len(item.alphabet)):
                next_alph: list[str] = copy(item.alphabet)
                char: str = next_alph.pop(index)
                stack.append(Item(next_alph, item.out_row + char))
        else:
            yield item.out_row


if __name__ == '__main__':
    alphabet: str = 'ABCD'
    answer: set[str] = {
        'DCBA', 'DCAB', 'DBCA', 'DBAC', 'DACB', 'DABC', 'CDBA', 'CDAB', 'CBDA', 'CBAD', 'CADB', 'CABD',
        'BDCA', 'BDAC', 'BCDA', 'BCAD', 'BADC', 'BACD', 'ADCB', 'ADBC', 'ACDB', 'ACBD', 'ABDC', 'ABCD'
    }

    assert set(combine_with_recurse(alphabet)) == answer
    assert set(combine_with_stack(alphabet)) == answer
